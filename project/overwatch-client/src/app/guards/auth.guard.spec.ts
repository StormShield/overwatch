import { TestBed } from '@angular/core/testing';

import { AuthGuard } from './auth.guard';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth/auth.service';

describe('AuthGuard', () => {
  let guard: AuthGuard;

  // Spies
  let authServiceSpy;
  let routerSpy;
  let navigateSpy;

  beforeEach(() => {   
    // Declare spies
    authServiceSpy = jasmine.createSpyObj('AuthService', ['isLoggedIn']);
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

    navigateSpy = routerSpy.navigateByUrl.and.returnValue(Promise.resolve(true));

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        // Inject spies service
        { provide: Router, useValue: routerSpy }, 
        { provide: AuthService, useValue: authServiceSpy },
      ],
    });
    guard = TestBed.inject(AuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });

  /*
    Test if user authenticate then redirect to home page
  */
 it('should redirect to home page', () => {

  // Login spy
  const isLoggedIn = authServiceSpy.isLoggedIn.and.returnValue(true);

  // Call guard
  const canActivate = guard.canActivate();

  expect(isLoggedIn).toHaveBeenCalled();
  expect(canActivate).toBeFalse();
  expect(navigateSpy).toHaveBeenCalledWith('/home');
})

/*
  Test if not authenticate then stay on page and return true
*/
it('should stay on page', () => {
  // Login spy
  const isLoggedIn = authServiceSpy.isLoggedIn.and.returnValue(false);

  // Call guard
  const canActivate = guard.canActivate();

  expect(isLoggedIn).toHaveBeenCalled();
  expect(canActivate).toBeTrue();
  expect(navigateSpy).toHaveBeenCalledTimes(0);
})
});
