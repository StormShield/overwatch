import { TestBed } from '@angular/core/testing';

import { LoggedGuard } from './logged.guard';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('LoggedGuard', () => {
  let guard: LoggedGuard;

    // Spies
    let authServiceSpy;
    let routerSpy;
    let navigateSpy;

  beforeEach(() => {

    // Declare spies
    authServiceSpy = jasmine.createSpyObj('AuthService', ['isLoggedIn']);
    routerSpy = jasmine.createSpyObj('AuthService', ['navigateByUrl']);

    navigateSpy = routerSpy.navigateByUrl.and.returnValue(Promise.resolve(true));

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        // Inject spies service
        { provide: Router, useValue: routerSpy }, 
        { provide: AuthService, useValue: authServiceSpy },
      ],
    });
    guard = TestBed.inject(LoggedGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });


  /*
    Test if not auhtenticat then redirect to login page
  */
  it('should redirect to login page', () => {

    // Login spy
    const isLoggedIn = authServiceSpy.isLoggedIn.and.returnValue(false);

    // Call guard
    const canActivate = guard.canActivate();

    expect(isLoggedIn).toHaveBeenCalled();
    expect(canActivate).toBeFalse();
    expect(navigateSpy).toHaveBeenCalledWith('/login');
  })

  /*
    Test if authenticate then stay on page and return true
  */
 it('should stay on page', () => {
    // Login spy
    const isLoggedIn = authServiceSpy.isLoggedIn.and.returnValue(true);

    // Call guard
    const canActivate = guard.canActivate();

    expect(isLoggedIn).toHaveBeenCalled();
    expect(canActivate).toBeTrue();
    expect(navigateSpy).toHaveBeenCalledTimes(0);
  })
});
