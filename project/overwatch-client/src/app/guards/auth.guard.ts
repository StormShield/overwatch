import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }


  /**
   * Can activate only if the user is not logged. 
   * 
   * If the user is logged then redirect to home page
   *
   * @returns
   * @memberof AuthGuard
   */
  canActivate() {
    if (this.authService.isLoggedIn()) {
      this.router.navigateByUrl('/home');
    }
    return !this.authService.isLoggedIn();
  }
  
}
