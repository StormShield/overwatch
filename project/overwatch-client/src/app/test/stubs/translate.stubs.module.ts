import { Injectable, PipeTransform, NgModule, EventEmitter, Pipe } from '@angular/core'
import { of } from 'rxjs'
import { TranslateService } from '@ngx-translate/core'


/**
 * Stub of the translate pipe
 *
 * @export
 * @class TranslatePipeStub
 * @implements {PipeTransform}
 */
@Injectable()
@Pipe({ name: "translate" })
export class TranslatePipeStub implements PipeTransform {
  public transform(key: string, ...args: any[]): any { return key }
}

/**
 * Stub of the translate service
 *
 * @export
 * @class TranslationServiceStub
 */
@Injectable()

export class TranslationServiceStub {
  public onLangChange = new EventEmitter<any>()
  public onTranslationChange = new EventEmitter<any>()
  public onDefaultLangChange = new EventEmitter<any>()
  public addLangs(langs: string[]) { return }
  public getLangs() { return ["en-us"] }
  public getBrowserLang() { return "" }
  public getBrowserCultureLang() { return "" }
  public use(lang: string) { return null }
  // tslint:disable-next-line:no-reserved-keywords
  public get(key: any): any { return of(key) }
  public setDefaultLang(lang: string): void {}
  public instant(key: string): string {return ''}
}


/**
 * Translate module to be use in different test
 *
 * @export
 * @class TranslateStubsModule
 */
@NgModule({
    declarations: [
      TranslatePipeStub,
    ],
    exports: [
      TranslatePipeStub,
    ],
    providers: [
      { provide: TranslateService, useClass: TranslationServiceStub },
    ],
  })
  export class TranslateStubsModule {}