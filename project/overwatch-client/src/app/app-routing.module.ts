import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { AuthGuard } from './guards/auth.guard';
import { LoggedGuard } from './guards/logged/logged.guard';
import { MaintenanceComponent } from './components/maintenance/maintenance.component';
import { AnomalyComponent } from './components/anomaly/anomaly.component';

const routes: Routes = [
  { 
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  { path: 'login', component: LoginComponent, canActivate: [AuthGuard]},
  { 
    path: 'home', 
    component: HomeComponent, 
    canActivate: [LoggedGuard],
    children: [
      {
          path: 'maintenance',
          component: MaintenanceComponent,
          canActivate: [LoggedGuard]
      },
      {
        path: 'anomaly',
        component: AnomalyComponent,
        canActivate: [LoggedGuard]
      }
    ]
  },
];



@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
