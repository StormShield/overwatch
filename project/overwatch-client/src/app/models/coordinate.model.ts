

/**
 * Coordinate on three dimensions
 *
 * @export
 * @class Coordinate
 */
export class Coordinate {
    x: number;
    y: number;
    z: number;
}
