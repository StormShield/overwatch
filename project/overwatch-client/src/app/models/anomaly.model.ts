import { Coordinate } from './coordinate.model';






/**
 * Enum of the different existing default type
 *
 * @export
 * @enum {number}
 */
export enum DefaultTypeEnum {
    PEELING = 'Peeling',
    POROSITY = 'Porosity',
    CRACK = 'Crack'
}



/**
 * Anomaly on a plane
 *
 * @export
 * @class Anomaly
 */
export class Anomaly {
    coordinate: Coordinate; // Coordinate of the anomaly
    defaultType: DefaultTypeEnum;
    comment: string

    constructor() {
        this.coordinate = new Coordinate();
    }
}