


/**
 * Tokens model 
 * Contains access and refresh token
 *
 * @export
 * @class Tokens
 */
export class Tokens {
    access: string; // Access token
    refresh: string; // Refresh token 
}