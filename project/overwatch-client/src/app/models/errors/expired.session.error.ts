import { HttpErrorResponse } from '@angular/common/http';




/**
 * Custom http error expired session
 * 
 * Should be use when to refresh token has expired
 *
 * @export
 * @class ExpiredSessionError
 * @extends {HttpErrorResponse}
 */
export class ExpiredSessionError extends HttpErrorResponse{

    /**
     *Creates an instance of ExpiredSessionError.
     * @memberof ExpiredSessionError
     */
    constructor() {
        super({
            status: 401,
            statusText: 'httpException.expiredSession.statusText',
            error: 'httpException.expiredSession.error',
        });
    }
}