import { HttpErrorResponse } from '@angular/common/http';



/**
 * Custom http error bad credentials error
 * 
 * Should be use when the api/token request have 401 http error
 *
 * @export
 * @class BadCredentialsError
 * @extends {HttpErrorResponse}
 */
export class BadCredentialsError extends HttpErrorResponse {

    /**
     *Creates an instance of BadCredentialsError.
     * @memberof BadCredentialsError
     */
    constructor() {
        super({
            status: 401,
            statusText: 'httpException.badCredentials.statusText',
            error: 'httpException.badCredentials.error',
        });
    }
}