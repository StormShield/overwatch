import { HttpErrorResponse } from '@angular/common/http';




/**
 * Custom http error maintenace
 * 
 * Should be use when the server is in maintenance
 *
 * @export
 * @class ExpiredSessionError
 * @extends {HttpErrorResponse}
 */
export class MaintenanceError extends HttpErrorResponse {


    constructor() {
        super({
            status: 503,
            statusText: 'httpException.maintenance.statusText',
            error: 'httpException.maintenance.error',
        });
    }
}