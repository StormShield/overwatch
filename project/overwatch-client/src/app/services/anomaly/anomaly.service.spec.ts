import { TestBed } from '@angular/core/testing';

import { AnomalyService } from './anomaly.service';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { Anomaly, DefaultTypeEnum } from 'src/app/models/anomaly.model';
import { environment } from 'src/environments/environment';
import { FakeMissingTranslationHandler } from '@ngx-translate/core';

describe('AnomalyService', () => {
  let service: AnomalyService;

  let httpClient: HttpClient;
  let httpMock: HttpTestingController;

  // Fake model
  let fakeAnomaly: Anomaly;

  beforeEach(() => {

      // Declare model
      fakeAnomaly = new Anomaly()
      fakeAnomaly.coordinate.x = 500;
      fakeAnomaly.coordinate.y = 500;
      fakeAnomaly.coordinate.z = 500;
      fakeAnomaly.defaultType = DefaultTypeEnum.PEELING;
      fakeAnomaly.comment = 'this is my comment';

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });

    service = TestBed.inject(AnomalyService);
    httpClient = TestBed.get(HttpClient);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /*
    Test should create by sending a http request
  */
  it('should create anomaly sucess', () => {
    // Call create
    service.create(fakeAnomaly).subscribe((response: Anomaly) => {
      expect(response).toBeTruthy();
      expect(response.coordinate.x).toEqual(fakeAnomaly.coordinate.x);
      expect(response.coordinate.y).toEqual(fakeAnomaly.coordinate.y);
      expect(response.coordinate.z).toEqual(fakeAnomaly.coordinate.z);
      expect(response.defaultType).toEqual(fakeAnomaly.defaultType);
      expect(response.comment).toEqual(fakeAnomaly.comment);
    });

    //Request should exists
    const testReq: TestRequest = httpMock.expectOne(environment.apiUrl +  environment.urls.anomaly);
    testReq.flush(fakeAnomaly);
  });

  /*
    Test should create by sending a http request and rethrowing the error
  */
 it('should create anomaly fail', () => {
    const expectedError = { status: 401, statusText: 'My custom status text'};

    // Call create
    service.create(fakeAnomaly).subscribe((response: Anomaly) => {
      fail('The method should rethrow the http error')
    }, 
    (err) => {
      expect(err.status).toEqual(401);
    });

    //Request should exists
    const testReq: TestRequest = httpMock.expectOne(environment.apiUrl +  environment.urls.anomaly);

    // Respond with mock 401 
    testReq.flush('Error', expectedError);
  });
});
