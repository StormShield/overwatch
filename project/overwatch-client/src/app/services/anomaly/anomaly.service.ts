import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Anomaly } from 'src/app/models/anomaly.model';

@Injectable({
  providedIn: 'root'
})
export class AnomalyService {

  constructor(private http: HttpClient) {}



  /**
   * Get all anomalies
   *
   * @returns {Observable<any>}
   * @memberof AnomalyService
   */
  getAll(): Observable<any> {
    return this.http.get<any>(environment.apiUrl + environment.urls.anomaly);
  }



  /**
   * Create an anomly in database
   * 
   *
   * @param {Anomaly} anomaly : to create
   * @returns {Observable<Anomaly>} : the anomaly created
   * @memberof AnomalyService
   */
  create(anomaly: Anomaly): Observable<Anomaly> {
    const url = environment.apiUrl + environment.urls.anomaly;
    return this.http.post<Anomaly>(url, anomaly);
  }



}
