import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { mapTo, tap, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Tokens } from 'src/app/models/tokens.model';
import * as jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly ACCESS_TOKEN = 'ACCESS_TOKEN';
  private readonly REFRESH_TOKEN = 'REFRESH_TOKEN';
  private readonly IS_ADMIN = 'IS_ADMIN'

  
  constructor(private http: HttpClient, private router: Router) {}


  /**
   * Get the jwt and refresh token from server and store it in the session cookie (local storage)
   * 
   * Send an notification message in case of error
   *
   * @param {{ username: string, password: string }} user
   * @returns {Observable<boolean>} : the observable return false in case of error. True otherwise
   * @memberof AuthService
   */
  login(user: { username: string, password: string }): Observable<boolean> {
    return this.http.post<any>(environment.apiUrl + environment.urls.accessToken, user)
      // Tap to handle side effects
      .pipe(tap(tokens => this.doLoginUser(user.username, tokens)),
        // Got the token      
        mapTo(true),
        // Handle error
        catchError(error => {
          return throwError(error);
        }));
  }


  /**
   * Logout the user then redirect to login page
   * 
   * By deleting the local storage
   *

   * @memberof AuthService
   */
  logout() {
    this.doLogoutUser();
    this.router.navigateByUrl('/login');
    
  }


  /**
   *Refresh the JWT token by using the refresh tolen
   *
   * @returns {Observable<Tokens>} : the refresh token
   * @memberof AuthService
   */
  refreshToken(): Observable<Tokens> {
    return this.http.post<any>(environment.apiUrl + '' + environment.urls.refreshToken, {'refresh': this.getRefreshToken()}).pipe(
      tap((tokens: Tokens) => {
        // Store new access token
        this.storeJwtToken(tokens.access);
      })
    );
  }



  /**
   * Check if a user is logged by checkin JWT token in local storage
   *
   * @returns {boolean} : true is the user a user if logged. False otherwise
   * @memberof AuthService
   */
  isLoggedIn(): boolean {
    return !!this.getJwtToken();
  }


  /**
   * Get the role of the current user.
   *
   * @returns {boolean} : true if admin, false otherwise
   * @memberof AuthService
   */
  isAdmin(): boolean {
    const isAdmin: boolean = localStorage.getItem(this.IS_ADMIN) === 'true';
    return isAdmin;
  }

    /**
   * Do login user
   * Save the username in the service and store tokens in local storage
   *
   * @private
   * @param {string} username
   * @param {Tokens} tokens
   * @memberof AuthService
   */
  private doLoginUser(username: string, tokens: Tokens) {
    this.storeTokens(tokens);
  }


  /**
   * Logout the user
   * Remove the user from service and delete the tokens in local storage
   *
   * @private
   * @memberof AuthService
   */
  private doLogoutUser() {
    this.removeTokens();
  }

   /**
   * Get the jwt token from local storage
   *
   * @returns {string} : the jwt token
   * @memberof AuthService
   */
  getJwtToken(): string {
    return localStorage.getItem(this.ACCESS_TOKEN);
  }



  /**
   * Get the refresh token from local storage
   *
   * @private
   * @returns {string}
   * @memberof AuthService
   */
  private getRefreshToken(): string {
    return localStorage.getItem(this.REFRESH_TOKEN);
  }



  /**
   * Store the jwt token in 
   *
   * @private
   * @param {string} jwt
   * @memberof AuthService
   */
  private storeJwtToken(jwt: string) {
    localStorage.setItem(this.ACCESS_TOKEN, jwt);
  }


  /**
   * Store tokens in the local storage
   *
   * @private
   * @param {Tokens} tokens
   * @memberof AuthService
   */
  private storeTokens(tokens: Tokens) {
    localStorage.setItem(this.ACCESS_TOKEN, tokens.access);
    localStorage.setItem(this.REFRESH_TOKEN, tokens.refresh);
    localStorage.setItem(this.IS_ADMIN, String(this.getIsAdminFromJwtToken(tokens.access)));
  }

  

  /**
   * Remove tokens from local storage
   *
   * @private
   * @memberof AuthService
   */
  private removeTokens() {
    localStorage.removeItem(this.ACCESS_TOKEN);
    localStorage.removeItem(this.REFRESH_TOKEN);
    localStorage.removeItem(this.IS_ADMIN);
  }


  /**
   * Get the isAdmin user information from the jwt token
   * 
   *
   * @private
   * @param {string} jwtToken : should not be null
   * @returns {boolean} : return true is the user isAdmin. False otherwise
   * @memberof AuthService
   */
  private getIsAdminFromJwtToken(jwtToken: string): boolean {
    const decodeToken = jwt_decode(jwtToken);
    return decodeToken['isAdmin'] ? decodeToken['isAdmin'] : false ;
  }
  
}
