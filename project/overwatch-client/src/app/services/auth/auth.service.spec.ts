import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Tokens } from 'src/app/models/tokens.model';
import * as jwt_decode from 'jwt-decode';
import { access } from 'fs';

describe('AuthServiceService', () => {
  let service: AuthService;

  let httpClient: HttpClient;
  let httpMock: HttpTestingController;

  // Spies
  let routerSpy;
  let navigateSpy;

  // fake token
  const tokens: Tokens = new Tokens( );
  const fakeUser = { username: 'fakeuser', password: 'fakepwd'};

  // Mock local storage
  let mockLocalStorage;

  beforeEach(() => {
    // Create custom mock of the local storage
    let store = {};
    mockLocalStorage = {
      getItem: (key: string): string => {
        return key in store ? store[key] : null;
      },
      setItem: (key: string, value: string) => {
        store[key] = `${value}`;
      },
      removeItem: (key: string) => {
        delete store[key];
      },
      clear: () => {
        store = {};
      }
    };

    // Populate token
    // Access token isAdmin = true
    tokens.access = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTgzMzMwOTE2LCJqdGkiOiJmN2RmNDk3N2RlMjQ0MGFmYWQwMTZmYzJiMTA1M2M0NSIsInVzZXJfaWQiOjEsInVzZXJuYW1lIjoib3ZlcndhdGNoIiwiaXNBZG1pbiI6dHJ1ZX0.YW7ViwnnL6cVt-dUX6KgZ1-kaCRz3vQvsypLpXpFyew';
    tokens.refresh = 'myrefreshtoken';

    // Declare spies
    routerSpy = jasmine.createSpyObj('AuthService', ['navigateByUrl']);


    // TODO : shoudld mock the jwt_decode function
    // const funcSpy = jasmine.createSpy('jwt_decode').and.returnValue('myMockReturnValue');
    // const test = spyOnProperty(jwt_decode, 'jwt_decode', 'get').and.returnValue(funcSpy);

    navigateSpy = routerSpy.navigateByUrl.and.returnValue(Promise.resolve(true));

    localStorage.setItem = jasmine.createSpy().and.callFake(mockLocalStorage.setItem);
    localStorage.getItem = jasmine.createSpy().and.callFake(mockLocalStorage.getItem);
    localStorage.removeItem = jasmine.createSpy().and.callFake(mockLocalStorage.removeItem);

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        // Inject spies service
        { provide: Router, useValue: routerSpy },
      ],
    });


    service = TestBed.inject(AuthService);
    httpClient = TestBed.get(HttpClient);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /*
    Test the success logout
    By removing the tokens from storage
  */
  it('should logout', () => {
    service.logout();

    // Check remove storage
    expect(localStorage.removeItem).toHaveBeenCalledTimes(3);
    expect(localStorage.removeItem).toHaveBeenCalledWith('ACCESS_TOKEN');
    expect(localStorage.removeItem).toHaveBeenCalledWith('REFRESH_TOKEN');
    expect(localStorage.removeItem).toHaveBeenCalledWith('IS_ADMIN');

    // Navigate to login
    expect(navigateSpy).toHaveBeenCalledWith('/login');
  });

  /*
    Test the success of the login
    By storing the tokens logged the user
  */
  it('should login success', () => {
    service.login(fakeUser).subscribe(response => {
      expect(response).toBeTrue();

      // Check local storate populate
      expect(localStorage.setItem).toHaveBeenCalledTimes(3);
      expect(localStorage.setItem).toHaveBeenCalledWith('ACCESS_TOKEN', tokens.access);
      expect(localStorage.setItem).toHaveBeenCalledWith('REFRESH_TOKEN', tokens.refresh);
      expect(localStorage.setItem).toHaveBeenCalledWith('IS_ADMIN', 'true');
    });

    //Request should exists
    const reqFake: TestRequest = httpMock.expectOne(environment.apiUrl +  environment.urls.accessToken);

    // Respond with mock 201 
    reqFake.flush(tokens, { status: 200, statusText: '200 OK' });
  });

  /*
  Test the fail login
  should just rethrow the error
  */
  it('should login fail', () => {
    const expectedError = { status: 401, statusText: 'My custom status text'};

    service.login(fakeUser).subscribe(response => {
      fail('Should never received the response of the http post')
    },
    (err) => {
      expect(err.status).toEqual(expectedError.status);
      expect(err.statusText).toEqual(expectedError.statusText);
    });

    //Request should exists
    const reqFake: TestRequest = httpMock.expectOne(environment.apiUrl + environment.urls.accessToken);

    // Respond with mock 401 
    reqFake.flush('Error', expectedError);

  });


  /*
    Test the success of refresh token
    By storing the new access token 
  */
 it('should refresh token success', () => {
    // new token after fake refresh
    const newTokens = new Tokens();
    newTokens.access = 'mynewtoken';
    service.refreshToken().subscribe((response: Tokens) => {
      expect(response.access).toEqual(newTokens.access);


      // Check local storate populate
      expect(localStorage.setItem).toHaveBeenCalledTimes(1);
      expect(localStorage.setItem).toHaveBeenCalledWith('ACCESS_TOKEN', newTokens.access);
    });

    //Request should exists
    const reqFake: TestRequest = httpMock.expectOne(environment.apiUrl + environment.urls.refreshToken);

    // Respond with mock 201 

    reqFake.flush(newTokens, { status: 200, statusText: '200 OK' });
  });

  /*
    Test isLoggedIn
    should be true if the access token is store on local storage
    should be falsse if the access token is not store on local storage
  */
  it('should isLoggedIn', () => {

    //test is not logged
    let result = service.isLoggedIn();
    expect(result).toBeFalse();
    expect(localStorage.getItem).toHaveBeenCalledWith('ACCESS_TOKEN');

    // Change storage
    localStorage.getItem = jasmine.createSpy().and.returnValue('mytoken');

    //test is logged
    result = service.isLoggedIn();
    expect(result).toBeTrue();
    expect(localStorage.getItem).toHaveBeenCalledWith('ACCESS_TOKEN');
  });

    /*
    Test isLoggedIn
    should be true if isAdmin is true in local storage
    should be false if isAdmin is false in local storage
    should be false if isAdmin doesn't exist in local storate
  */
 it('should isAdmin', () => {
    //test is not isAdmin in local storage
    let result = service.isAdmin();
    expect(result).toBeFalse();
    expect(localStorage.getItem).toHaveBeenCalledWith('IS_ADMIN');

    // Change storage
    localStorage.getItem = jasmine.createSpy().and.returnValue('true');

    //test is is admin in storage
    result = service.isAdmin();
    expect(result).toBeTrue();
    expect(localStorage.getItem).toHaveBeenCalledWith('IS_ADMIN');

    // Change storage
    localStorage.getItem = jasmine.createSpy().and.returnValue('false');

    //test is is not admin in storage
    result = service.isAdmin();
    expect(result).toBeFalse();
    expect(localStorage.getItem).toHaveBeenCalledWith('IS_ADMIN');
  });
  
});
