import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import {  Maintenance } from 'src/app/models/maintenance.model';

@Injectable({
  providedIn: 'root'
})
export class MaintenanceService {

  constructor(private http: HttpClient) { }


  /**
   * Set the maintenant status on server
   *
   * @param {boolean} isOn : the new maintenace status to be set
   * @returns {Observable<void>} : Observable response
   * @memberof MaintenanceService
   */
  setMaintenanceStatus(isOn: boolean): Observable<void> {
    // Build url
    const url = environment.apiUrl + environment.urls.maintenance + (isOn ? 'on' : 'off');
    return this.http.put<void>(url, {});
  }


  /**
   * Get the maitenance status of the server
   *
   * @returns {Observable<Maintenance>}
   * @memberof MaintenanceService
   */
  getMaintenanceStatus(): Observable<Maintenance> {
    // Build url
    const url = environment.apiUrl + environment.urls.maintenance;
    return this.http.get<Maintenance>(url);
  }
}
