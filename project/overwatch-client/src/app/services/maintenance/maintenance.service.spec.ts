import { TestBed } from '@angular/core/testing';

import { MaintenanceService } from './maintenance.service';
import { HttpClient } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule, TestRequest } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { Maintenance } from 'src/app/models/maintenance.model';

describe('MaintenanceService', () => {
  let service: MaintenanceService;

  let httpClient: HttpClient;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(MaintenanceService);
    httpClient = TestBed.get(HttpClient);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  /*
    Test sending request for set maintenacce status
  */
  it('should set maintenance status', () => {
    // call method 'on'
    service.setMaintenanceStatus(true).subscribe(() => {
    });

    //Request should exists
    let testReq: TestRequest = httpMock.expectOne(environment.apiUrl +  environment.urls.maintenance + 'on');
    expect(testReq.request.url).toEqual(environment.apiUrl +  environment.urls.maintenance + 'on');
    testReq.flush('success');

    // call method 'off'
    service.setMaintenanceStatus(false).subscribe(() => {
    });

    //Request should exists
    testReq = httpMock.expectOne(environment.apiUrl +  environment.urls.maintenance + 'off');
    expect(testReq.request.url).toEqual(environment.apiUrl +  environment.urls.maintenance + 'off');
    testReq.flush('success');
  })


  /*
    Test the sending request to get the maitnenance status
   */
  it('should get status maintenance', () => {
    // Fake status
    const fakeResult = new Maintenance();
    fakeResult.status = 'off';

    // call method get
    service.getMaintenanceStatus().subscribe((maitenance: Maintenance) => {
      expect(maitenance.status).toEqual('off');
    });

      //Request should exists
      let testReq: TestRequest = httpMock.expectOne(environment.apiUrl +  environment.urls.maintenance);
      expect(testReq.request.url).toEqual(environment.apiUrl +  environment.urls.maintenance);
      testReq.flush(fakeResult, {status: 200, statusText:'OK'});
  })

});
