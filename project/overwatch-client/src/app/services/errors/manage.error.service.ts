import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { TranslateService } from '@ngx-translate/core';
import { not } from '@angular/compiler/src/output/output_ast';


/**
 * Notification service. To be notify by the notificationService
 *
 * @export
 * @class NotificationMessage
 */
export class NotificationMessage {
    title: string;
    message: string;
}

@Injectable({
  providedIn: 'root'
})
export class ManageErrorService {

  constructor(private translateService: TranslateService) { }




  /**
   * Format the message to be notify
   *
   * @param {HttpErrorResponse} error
   * @returns {NotificationMessage}
   * @memberof ManageErrorService
   */
  getNotification(error: HttpErrorResponse): NotificationMessage {
      const notificationMessage = new NotificationMessage();
      try {
        notificationMessage.title = this.translateService.instant(error.statusText);
        notificationMessage.message = this.translateService.instant(error.error);
      } catch(e) {
        notificationMessage.title = error.statusText;
        notificationMessage.message = error.error;
      }

      notificationMessage.message = this.handleObjectResponse(notificationMessage.message);

      return notificationMessage;
  }


  /**
   * Parse the error message to be show on the HMI
   *
   * @private
   * @param {*} message
   * @returns
   * @memberof ManageErrorService
   */
  private handleObjectResponse(message: any) {
      let newMessage = '';

    // Array case
      if(Array.isArray(message)) {
        message.forEach(value => {
            newMessage += value + '\n';
        })
      }

      // Object Case
      else if(message instanceof Object) {
        const keyMessageList = Object.keys(message);
        for(let key of keyMessageList) {
            newMessage += key + ': ' + this.handleObjectResponse(message[key]);
        }
      }
      // Others => let like that
      else {
          newMessage = message;
      }
      return newMessage;
  }
}

