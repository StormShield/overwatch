import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCreateAnomalyComponent } from './modal-create-anomaly.component';
import { TranslateStubsModule } from 'src/app/test/stubs/translate.stubs.module';
import { AnomalyService } from 'src/app/services/anomaly/anomaly.service';
import { Anomaly, DefaultTypeEnum } from 'src/app/models/anomaly.model';
import { NotificationsService } from 'angular2-notifications';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs/internal/observable/of';
import { first } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

describe('ModalCreateAnomalyComponent', () => {
  let component: ModalCreateAnomalyComponent;
  let fixture: ComponentFixture<ModalCreateAnomalyComponent>;

  // Spes
  let anomalyServiceSpy;
  let notificationServiceSpy;
  let notificationErrorSpy;
  let notificationSuccessSpy;

  // Fake model
  let fakeAnomaly: Anomaly;

  beforeEach(async(() => {

    // Declare model
    fakeAnomaly = new Anomaly()
    fakeAnomaly.coordinate.x = 500;
    fakeAnomaly.coordinate.y = 500;
    fakeAnomaly.coordinate.z = 500;
    fakeAnomaly.defaultType = DefaultTypeEnum.PEELING;
    fakeAnomaly.comment = 'this is my comment';


    // Declare spies
    anomalyServiceSpy = jasmine.createSpyObj('AnomalyService', ['create']);
    notificationServiceSpy = jasmine.createSpyObj('NotificationsService', ['error', 'success']);

    notificationErrorSpy = notificationServiceSpy.error.and.returnValue(true);
    notificationSuccessSpy = notificationServiceSpy.success.and.returnValue(true);

    TestBed.configureTestingModule({
      imports: [
        TranslateStubsModule,
      ],
      declarations: [ ModalCreateAnomalyComponent ],
      providers: [
        { provide: AnomalyService, useValue: anomalyServiceSpy }, 
        { provide: NotificationsService, useValue: notificationServiceSpy }, 
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCreateAnomalyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // test that all HTML element are on view
  it('should htmt element form present', () => {
    expect(fixture.debugElement.query(By.css('#x-form'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('#y-form'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('#z-form'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('#default-type-form'))).toBeTruthy();
    expect(fixture.debugElement.query(By.css('#comment-form'))).toBeTruthy();
  })


  /*
    Test the form group
    Should have all parameter definie
  */
  it('should form group present with all good parameter', () => {
    //Get form group
    const form = component.anomalyForm;
    expect(form).toBeTruthy();

    //x
    expect(form.get('x')).toBeTruthy();
    expect(form.get('y')).toBeTruthy();
    expect(form.get('z')).toBeTruthy();
    expect(form.get('defaultType')).toBeTruthy();
    expect(form.get('comment')).toBeTruthy();
  })

  /*
    Test form should be invalid when empty
  */
  it('should form invalid when empty', () => {
    //Get form group
    const form = component.anomalyForm;
    form.controls.x.setValue('');
    form.controls.y.setValue('');
    form.controls.z.setValue('');
    form.controls.defaultType.setValue('');
    form.controls.comment.setValue('');
    expect(form.valid).toBeFalsy();
  });

  // X field validity
  it('x field validity', () => {
    const field = component.anomalyForm.controls.x;
    expect(field.valid).toBeFalsy();

    //max
    field.setValue(1001);
    expect(field.valid).toBeFalsy();

    //min
    field.setValue(400);
    expect(field.valid).toBeFalsy();

    // required
    field.setValue('');
    expect(field.hasError('required')).toBeTruthy();

    // required
    field.setValue(600);
    expect(field.valid).toBeTruthy();

  });

  // y field validity
  it('y field validity', () => {
    const field = component.anomalyForm.controls.y;
    expect(field.valid).toBeFalsy();

    //max
    field.setValue(1001);
    expect(field.valid).toBeFalsy();

    //min
    field.setValue(-200);
    expect(field.valid).toBeFalsy();

    // required
    field.setValue('');
    expect(field.hasError('required')).toBeTruthy();

    // required
    field.setValue(200);
    expect(field.valid).toBeTruthy();

  });

  // z field validity
  it('z field validity', () => {
    const field = component.anomalyForm.controls.z;
    expect(field.valid).toBeFalsy();

    //max
    field.setValue(1001);
    expect(field.valid).toBeFalsy();

    //min
    field.setValue(-1001);
    expect(field.valid).toBeFalsy();

    // required
    field.setValue('');
    expect(field.hasError('required')).toBeTruthy();

    // required
    field.setValue(600);
    expect(field.valid).toBeTruthy();

  });

  // default type field validity
  it('default type field validity', () => {
    const field = component.anomalyForm.controls.defaultType;
    expect(field.valid).toBeFalsy();

    // required
    field.setValue('');
    expect(field.hasError('required')).toBeTruthy();

    // required
    field.setValue('Peeling');
    expect(field.valid).toBeTruthy();

  });

  // comment type field validity
  it('comment type field validity', () => {
    const field = component.anomalyForm.controls.comment;
    expect(field.valid).toBeTruthy();

    // required
    field.setValue('');
    expect(field.hasError('required')).toBeFalsy();

    // required
    field.setValue('This is my comment');
    expect(field.valid).toBeTruthy();

  });

    /*
      Test form should be valid with good entrie
    */
  it('should form be valid with good entries', () => {
    //Get form group
    const form = component.anomalyForm;
    form.controls.x.setValue(500);
    form.controls.y.setValue(500);
    form.controls.z.setValue(500);
    form.controls.defaultType.setValue('Peeling');
    form.controls.comment.setValue('This is my comment');
    expect(form.valid).toBeTruthy();
  });


  /*
    Should call AnomalyService after submit sucess
  */
  it('should post anomaly after sumbit success', () => {
    // Check the ouput emmitter will be triger at the end
    component.onClose.pipe(first()).subscribe((anomalyCreated: boolean) => {
      expect(anomalyCreated).toEqual(true);
    })

    // Populate form
    const form = component.anomalyForm;
    form.controls.x.setValue(500);
    form.controls.y.setValue(500);
    form.controls.z.setValue(500);
    form.controls.defaultType.setValue('Peeling');
    form.controls.comment.setValue('This is my comment');

    // Mock
    const createSpy = anomalyServiceSpy.create.and.returnValue(of(fakeAnomaly));

    // Call submit
    component.submit();

    // Check the call of anomaly.create with good anomaly object
    expect(createSpy).toHaveBeenCalledWith(jasmine.objectContaining({
      coordinate: jasmine.objectContaining({x:500, y:500, z:500}),
      defaultType: 'Peeling',
      comment: 'This is my comment'
    }));

    // Expect notification
    expect(notificationSuccessSpy).toHaveBeenCalled();

    // Expect reset form group
    expect(form.controls.x.value).toEqual(null);
    expect(form.controls.y.value).toEqual(null);
    expect(form.controls.z.value).toEqual(null);
    expect(form.controls.defaultType.value).toEqual(null);
    expect(form.controls.comment.value).toEqual(null);
  })

  /*
    Should call AnomalyService after submit fail
  */
 it('should post anomaly after sumbit fail', () => {
    // Check the ouput emmitter should never be trigger after fail
    component.onClose.pipe(first()).subscribe((anomalyCreated: boolean) => {
      fail('The onClose emitter should not be trigger after an http error')
    })

    // Populate form
    const form = component.anomalyForm;
    form.controls.x.setValue(500);
    form.controls.y.setValue(500);
    form.controls.z.setValue(500);
    form.controls.defaultType.setValue('Peeling');
    form.controls.comment.setValue('This is my comment');

    // Mock
    const createSpy = anomalyServiceSpy.create.and.returnValue(throwError(new HttpErrorResponse({status:401, statusText:'Unauthorized'})));

    // Call submit
    component.submit();

    // Check the call of anomaly.create with good anomaly object
    expect(createSpy).toHaveBeenCalledWith(jasmine.objectContaining({
      coordinate: jasmine.objectContaining({x:500, y:500, z:500}),
      defaultType: 'Peeling',
      comment: 'This is my comment'
    }));

    // Expect notification
    expect(notificationSuccessSpy).toHaveBeenCalledTimes(0);
    expect(notificationErrorSpy).toHaveBeenCalledTimes(1);


    // Expect form group has not be reset
    expect(form.controls.x.value).toEqual(500);
    expect(form.controls.y.value).toEqual(500);
    expect(form.controls.z.value).toEqual(500);
    expect(form.controls.defaultType.value).toEqual('Peeling');
    expect(form.controls.comment.value).toEqual('This is my comment');
  })
});
