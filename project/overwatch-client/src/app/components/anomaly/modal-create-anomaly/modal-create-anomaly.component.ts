import { Component, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { Anomaly } from 'src/app/models/anomaly.model';
import { ClrForm, ClrLoadingState } from '@clr/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AnomalyService } from 'src/app/services/anomaly/anomaly.service';
import { HttpErrorResponse } from '@angular/common/http/http';
import { NotificationsService } from 'angular2-notifications';
import { TranslateService } from '@ngx-translate/core';
import { ManageErrorService, NotificationMessage } from 'src/app/services/errors/manage.error.service';

@Component({
  selector: 'app-modal-create-anomaly',
  templateUrl: './modal-create-anomaly.component.html',
  styleUrls: ['./modal-create-anomaly.component.css']
})
export class ModalCreateAnomalyComponent implements OnInit {
  
  // Get anomaly form
  @ViewChild(ClrForm, {static: true}) clrForm;

  // Emit event when the modal has to be submit
  // Send a boolean that indicate if an anomaly has been created
  @Output() onClose: EventEmitter<boolean> = new EventEmitter<boolean>();

  // true if the modal is open
  @Input() isOpen: boolean;

  // The status of the submit button
  submitBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;

  anomalyForm: FormGroup;

  constructor(private anomalyService: AnomalyService, private notificationService: NotificationsService, private translateService: TranslateService, private erroService: ManageErrorService) { }


  /**
   * On init component 
   * Create anomaly form group
   *
   * @memberof ModalCreateAnomalyComponent
   */
  ngOnInit(): void {
    // The form group control associate to our clarity form
    this.anomalyForm = new FormGroup({
      x: new FormControl('', [Validators.required, Validators.min(500), Validators.max(1000), Validators.pattern(/^([-]?)+[0-9]+(.[0-9]{0,2})?$/)]), // Regex to have only 2 decimals max
      y: new FormControl('', [Validators.required, Validators.min(0), Validators.max(500), Validators.pattern(/^([-]?)+[0-9]+(.[0-9]{0,2})?$/)]),
      z: new FormControl('', [Validators.required, Validators.min(-1000), Validators.max(1000), Validators.pattern(/^([-]?)+[0-9]+(.[0-9]{0,2})?$/)]),
      defaultType: new FormControl('', Validators.required),
      comment: new FormControl('')
    });
  }

  
  /**
   * On cancel modal
   * @param {boolean} anomalyCreayed
   * @memberof ModalCreateAnomalyComponent
   */
  onCancel(anomalyCreayed: boolean) {
    this.anomalyForm.reset();
    this.onClose.emit(anomalyCreayed);
  }


  /**
   * Submit the form and send the data to the server
   *
   * @memberof ModalCreateAnomalyComponent
   */
  submit() {
    this.submitBtnState = ClrLoadingState.LOADING; 
    const model = this.getModel();
    if(model) {
      // Send request
      this.anomalyService.create(model).subscribe((anomaly: Anomaly) => {
        this.submitBtnState = ClrLoadingState.SUCCESS; 
        this.onCancel(true);
        this.notificationService.success(this.translateService.instant('anomaly.create.createsuccess'));
      },
      (err: HttpErrorResponse) => {
        this.submitBtnState = ClrLoadingState.ERROR; 
        const notification: NotificationMessage = this.erroService.getNotification(err);
        this.notificationService.error(notification.title, notification.message);
      });
    }
    else {
      this.submitBtnState = ClrLoadingState.DEFAULT; 
    }
    
  }


  /**
   * Get anomaly model using the form group model
   *
   * @private
   * @returns Anomlay or null if the anomalyForm is not defined or not valid
   * @memberof ModalCreateAnomalyComponent
   */
  private getModel() {
    if(this.anomalyForm && this.anomalyForm.valid) {
      const anomalyModel: Anomaly = new Anomaly();;
      // Populate ou model
      anomalyModel.coordinate.x = this.anomalyForm.get('x').value;
      anomalyModel.coordinate.y = this.anomalyForm.get('y').value;
      anomalyModel.coordinate.z = this.anomalyForm.get('z').value;
      anomalyModel.defaultType = this.anomalyForm.get('defaultType').value;
      anomalyModel.comment = this.anomalyForm.get('comment').value;
      anomalyModel.comment = anomalyModel.comment === '' ? null : anomalyModel.comment;
      return anomalyModel;
    }
    else {
      return null;
    }
  }
}
