import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-anomaly',
  templateUrl: './anomaly.component.html',
  styleUrls: ['./anomaly.component.css']
})
export class AnomalyComponent implements OnInit {

  constructor() { }

  isCreateAnomalyModalOpen = false;

  ngOnInit(): void {
  }


  /**
   * Close the create anomaly modal
    *
   * @param {boolean} created : if an anomaly has been created
   * @memberof AnomalyComponent
   */
  onCloseCreateModal(created: boolean) {
    this.isCreateAnomalyModalOpen = false;
  }

}
