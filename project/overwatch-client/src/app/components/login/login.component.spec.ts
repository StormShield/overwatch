import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SimpleNotificationsModule, NotificationsService } from 'angular2-notifications';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { of, throwError } from 'rxjs';
import { BadCredentialsError } from 'src/app/models/errors/bad.credentials.error';
import { TranslateStubsModule } from 'src/app/test/stubs/translate.stubs.module';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  
  // Spies
  let authServiceSpy;
  let routerSpy;
  let notificationServiceSpy;
  let navigateSpy;
  let notificationErrorSpy;

  const fakeUser = { username: 'fakeuser', password: 'fakepwd'};

  beforeEach(async(() => {

    // Declare spies
    authServiceSpy = jasmine.createSpyObj('AuthService', ['login']);
    routerSpy = jasmine.createSpyObj('AuthService', ['navigateByUrl']);
    notificationServiceSpy = jasmine.createSpyObj('NotificationsService', ['error']);

    navigateSpy = routerSpy.navigateByUrl.and.returnValue(Promise.resolve(true));
    notificationErrorSpy = notificationServiceSpy.error.and.returnValue(true);


    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule,
        BrowserAnimationsModule,
        TranslateStubsModule,
        SimpleNotificationsModule.forRoot()],
      declarations: [ LoginComponent ],
      providers: [
        // Inject spies service
        { provide: Router, useValue: routerSpy }, 
        { provide: AuthService, useValue: authServiceSpy }, 
        { provide: NotificationsService, useValue: notificationServiceSpy }, 
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  /*
    Should create
   */
  it('should create', () => {
    expect(component).toBeTruthy();
  });



  /*
    Test do nothing if the credentials are not provide
   */
  it('should do nothing (no credentials)', () => {

    // Credentials should be undefined at first
    expect(component.username).toBe(undefined, 'undefined at first');
    expect(component.password).toBe(undefined, 'undefined at first');

    // Login spy
    const loginSpy = authServiceSpy.login.and.returnValue(of(true));

    // Call login
    component.login();

    // Auth service not call
    expect(loginSpy).toHaveBeenCalledTimes(0);
  });

  /*
    Test login with good credential
  */
  it('should navigate to home', () => {
    // Login spy
    const loginSpy = authServiceSpy.login.and.returnValue( of(true) );

    // Populate attribute
    component.username = fakeUser.username;
    component.password = fakeUser.password;

    // Call login
    component.login();

    // Auth service call
    expect(loginSpy).toHaveBeenCalledWith(fakeUser);

    // Expect redirection
    expect(navigateSpy).toHaveBeenCalledWith('/home')
  });


  /*
    Test error form authService
    Component should notify an error
  */
  it('should notify error', () => {
    // Login spy
    const badCreddentialsError = new BadCredentialsError();
    const loginSpy = authServiceSpy.login.and.returnValue( throwError(badCreddentialsError) );

    // Populate attribute
    component.username = fakeUser.username;
    component.password = fakeUser.password;

    // Call login
    component.login();

    // Auth service call
    expect(loginSpy).toHaveBeenCalledWith(fakeUser);

    // Expect redirection
    expect(navigateSpy).toHaveBeenCalledTimes(0);

    // Expect notification error
    expect(notificationErrorSpy).toHaveBeenCalled();
  })

});


