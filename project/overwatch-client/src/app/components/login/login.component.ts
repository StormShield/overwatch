import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service'
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { HttpErrorResponse } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { first } from 'rxjs/operators';
import { NotificationMessage, ManageErrorService } from 'src/app/services/errors/manage.error.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;

  constructor(private authService: AuthService, 
               private router: Router, 
               private notificationService: NotificationsService,
               private translateService: TranslateService,
               private erroService: ManageErrorService) {}

  ngOnInit(): void {
  }

  login() {
    if (this.username && this.password) {
      this.authService.login({username: this.username, password: this.password}).pipe(first())
          .subscribe(
              (loggedIn: boolean) => {
                if(loggedIn) {
                  this.router.navigateByUrl('/home');
                }
              },
              (err: HttpErrorResponse) => {
                const notification: NotificationMessage = this.erroService.getNotification(err);
                this.notificationService.error(notification.title, notification.message);
              }
          );
    }
  }

}
