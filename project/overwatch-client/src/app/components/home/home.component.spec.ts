import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { AuthService } from 'src/app/services/auth/auth.service';
import { NotificationsService, SimpleNotificationsModule } from 'angular2-notifications';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateStubsModule } from 'src/app/test/stubs/translate.stubs.module';
import { ClarityModule } from '@clr/angular';
import { FormsModule } from '@angular/forms';
import { LanguageInterceptor } from 'src/app/interceptors/language/language.interceptor';
import { MaintenanceService } from 'src/app/services/maintenance/maintenance.service';
import { of } from 'rxjs/internal/observable/of';
import { throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { By } from "@angular/platform-browser";



describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  // Spies
  let authServiceSpy;
  let maintenanceServiceSpy;

  let notificationServiceSpy;
  let notificationErrorSpy;
  let notificationSuccessSpy;

  let translateServiceSpy;
  let logoutSpy;

  beforeEach(async(() => {

    // Declare spies
    authServiceSpy = jasmine.createSpyObj('AuthService', ['logout', 'isAdmin']);
    maintenanceServiceSpy = jasmine.createSpyObj('MaintenanceService', ['setMaintenanceStatus']);

    notificationServiceSpy = jasmine.createSpyObj('NotificationsService', ['error', 'success']);
    notificationErrorSpy = notificationServiceSpy.error.and.returnValue(true);
    notificationSuccessSpy = notificationServiceSpy.success.and.returnValue(true);

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ClarityModule,
        FormsModule,
        BrowserAnimationsModule,
        TranslateStubsModule,
        SimpleNotificationsModule.forRoot()        
      ],
        

      declarations: [ HomeComponent],
      providers: [
        { provide: AuthService, useValue: authServiceSpy }, // Inject spy authService instead of the original one 
        { provide: MaintenanceService, useValue: maintenanceServiceSpy },
        { provide: NotificationsService, useValue: notificationServiceSpy },
      ], 

    })
    .compileComponents();
  }));

  beforeEach(() => {
    translateServiceSpy = jasmine.createSpyObj('TranslationServiceStub', ['use']);
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /*
    Test call logout
  */
  it('logout', () => {
    component.logout();
    expect(authServiceSpy.logout).toHaveBeenCalledTimes(1);
  })

  /*
    Test the switch lang by calling the translate service and store the preference language in localStorage
  */
  it('switchLang', () => {
    // Change storage
    localStorage.setItem = jasmine.createSpy().and.returnValue('void');
    component.switchLang('fr');
    expect(localStorage.setItem).toHaveBeenCalledWith(LanguageInterceptor.PREFERENCE_LANGUAGE, 'fr');
  })

  /*
    Test the change maitenant status 
    Should be call maintenanceService
    then notifify the user
  */
 it('Should change maintenance status', () => {
    // Success on
    maintenanceServiceSpy.setMaintenanceStatus = jasmine.createSpy().and.returnValue(of('void'));
    component.changeMaintenaceStatus(true);
    expect(maintenanceServiceSpy.setMaintenanceStatus).toHaveBeenCalledWith(true);
    expect(notificationSuccessSpy).toHaveBeenCalled();

    notificationSuccessSpy.calls.reset();
    maintenanceServiceSpy.setMaintenanceStatus.calls.reset();

    // Success off
    component.changeMaintenaceStatus(false);
    expect(maintenanceServiceSpy.setMaintenanceStatus).toHaveBeenCalledWith(false);
    expect(notificationSuccessSpy).toHaveBeenCalled();

    notificationSuccessSpy.calls.reset();
    maintenanceServiceSpy.setMaintenanceStatus.calls.reset();

    // Error
    maintenanceServiceSpy.setMaintenanceStatus = jasmine.createSpy().and.returnValue(throwError(new HttpErrorResponse({error: 'Custom error'})));
    component.changeMaintenaceStatus(false);
    expect(maintenanceServiceSpy.setMaintenanceStatus).toHaveBeenCalledWith(false);
    expect(notificationSuccessSpy).toHaveBeenCalledTimes(0);
    expect(notificationErrorSpy).toHaveBeenCalledTimes(1);

  })

  /*
    Test in admin mode that the maintenance button appear
  */
  it('should maintenance button appear', () => {
    component.isAdmin = true;
    fixture.detectChanges();
    const el = fixture.debugElement.query(By.css('#maintenace-conf'));
    expect(el).toBeDefined();
  })

  /*
    Test in basic user mode that the maintenance button should not appear
  */
  it('should maintenance button be hide', () => {
    component.isAdmin = false;
    fixture.detectChanges();
    const el = fixture.debugElement.query(By.css('#maintenace-conf'));
    expect(el).toBeNull();
  })
});
