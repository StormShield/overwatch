import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { NotificationsService } from 'angular2-notifications';
import { LanguageInterceptor } from 'src/app/interceptors/language/language.interceptor';
import { MaintenanceService } from 'src/app/services/maintenance/maintenance.service';
import { HttpErrorResponse } from '@angular/common/http';
import { AnomalyService } from 'src/app/services/anomaly/anomaly.service';
import { first } from 'rxjs/operators';
import { NotificationMessage, ManageErrorService } from 'src/app/services/errors/manage.error.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private readonly ACCESS_TOKEN = 'ACCESS_TOKEN';

  isAdmin = false;

  constructor(private authService: AuthService, 
    private notificationService: NotificationsService, 
    private translateService: TranslateService, 
    private maintenaceService: MaintenanceService,
    private erroService: ManageErrorService) { }

  ngOnInit(): void {
    this.isAdmin = this.authService.isAdmin();
  }


  /**
   * Logout the user
   *
   * @memberof HomeComponent
   */
  logout() {
    this.authService.logout();
  }


  /**
   * Change the language on the global app
   *
   * @param {string} lang
   * @memberof HomeComponent
   */
  switchLang(lang: string) {
    // Change on global service the parameter lang
    this.translateService.use(lang);
    // Store it on storage
    localStorage.setItem(LanguageInterceptor.PREFERENCE_LANGUAGE, lang)
  }


  /**
   * Change the maintenance status on server
   *
   * @param {boolean} isOn
   * @memberof HomeComponent
   */
  changeMaintenaceStatus(isOn: boolean) {
    this.maintenaceService.setMaintenanceStatus(isOn).pipe(first()).subscribe(() => {
      this.notificationService.success(this.translateService.instant('home.setMaintenanceSuccess'));
    },
    (err: HttpErrorResponse) => {
      const notification: NotificationMessage = this.erroService.getNotification(err);
      this.notificationService.error(notification.title, notification.message);
    });
  }

}
