import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, BehaviorSubject, throwError } from 'rxjs';
import { AuthService } from '../../services/auth/auth.service';
import { catchError, switchMap, filter, take, mergeMap, map } from 'rxjs/operators';
import { Tokens } from '../../models/tokens.model';
import { environment } from 'src/environments/environment';
import { ExpiredSessionError } from '../../models/errors/expired.session.error';
import { BadCredentialsError } from '../../models/errors/bad.credentials.error';



/**
 * Request Interceptor to inject jwt token in the header requests
 * 
 * see: https://angular-academy.com/angular-jwt/
 *
 * @export
 * @class TokenInterceptor
 * @implements {HttpInterceptor}
 */
@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(public authService: AuthService) { }

  private isRefreshing = false;
  private refreshTokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);


  /**
   * Intercept the http request to check the JWT token
   * 
   * 1) Inject the token to request if exist
   * 2) Send request
   * 3) If 401 error from server then handle it
   *
   * @param {HttpRequest<unknown>} request
   * @param {HttpHandler} next
   * @returns {Observable<HttpEvent<unknown>>}
   * @memberof TokenInterceptor
   */
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // Inject jewt token if exists
    if (this.authService.getJwtToken()) {
      request = this.addToken(request, this.authService.getJwtToken());
    }

    // Send the request
    return next.handle(request).pipe(
      // Handle error response
      catchError(error => {
        // 401 error
        if (error instanceof HttpErrorResponse && error.status === 401) {
          // If the error come from refresh token request then logout because the refresh token expired
          if(error.error && error.error.code === environment.httpErrorCode.invalid_token && request.url.includes(environment.urls.refreshToken)) {
            this.authService.logout();
            return throwError(new ExpiredSessionError());
          }

          // If error come from api/token request then it's bad credential error
          else if(request.url.includes(environment.urls.accessToken)) {
            return throwError(new BadCredentialsError());
          }

          // Otherwise handle the error
          else {
            return this.handle401Error(request, next);
          }

        } else {
          // Re throw error if its not 401 status message
          return throwError(error);
      }
    })

    );
  }



  /**
   * Add jwt token to the header request
   *
   * @private
   * @param {HttpRequest<unknown>} request : request to send to server
   * @param {string} token : jwt token
   * @returns {HttpRequest<unknown>} : return the request with the new header
   * @memberof TokenInterceptor
   */
  private addToken(request: HttpRequest<unknown>, token: string): HttpRequest<unknown> {
    return request.clone({
      setHeaders: {
        'Authorization': `Bearer ${token}`
      }
    });
  }


  /**
   * Handle 401 http error
   * 
   * Get a new access token by using the refresh token
   *
   * @private
   * @param {HttpRequest<any>} request
   * @param {HttpHandler} next
   * @returns {Observable<HttpEvent<unknown>>}
   * @memberof TokenInterceptor
   */
  private handle401Error(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // First 401 error
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next(null);

      // Call refresh token
      return this.authService.refreshToken().pipe(
        switchMap((token: Tokens) => {
          this.isRefreshing = false;
          this.refreshTokenSubject.next(token.access); // Trigger the subject to send the new access token
          return next.handle(this.addToken(request, token.access)); // Return the original http request with the new acces token injection
        })

      );
    }
    else {
      // For the next request that waiting for the new access token
      return this.refreshTokenSubject.pipe(
        filter(token => token != null), // Wait to have the access token
        take(1),
        switchMap(access => {
          return next.handle(this.addToken(request, access));
        }));
    }
  }
}
