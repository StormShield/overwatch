import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
  TestRequest,
} from '@angular/common/http/testing';

import { TokenInterceptor } from './token.interceptor';
import { AuthService } from '../../services/auth/auth.service';
import { HttpClient, HTTP_INTERCEPTORS, HttpErrorResponse } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { ExpiredSessionError } from '../../models/errors/expired.session.error';
import { environment } from 'src/environments/environment';
import { BadCredentialsError } from '../../models/errors/bad.credentials.error';

describe('TokenInterceptor', () => {

  let httpClient: HttpClient;
  let httpMock: HttpTestingController;

  // The fake token froml auth service
  const myFakeAccessToken = 'myfakeaccesstoken'
  const myFakeRefreshToken = 'myfakerefreshtoken'

  // The key of the header 'Authorization'
  const authorizationHeaderKey = 'Authorization';

  // Test url
  const testUrl = 'api/path'


  // Jasmine spies
  let authServiceSpy;
  let getJwtTokenSpy;
  let getRefreshToken;
  let logoutSpy;
  let refreshTokenSpy;

  beforeEach(() => {
    // Spy on auth service
    authServiceSpy = jasmine.createSpyObj('AuthService', ['getJwtToken', 'logout', 'getRefreshToken', 'refreshToken']);

    // GetJwtToken spy
    getJwtTokenSpy = authServiceSpy.getJwtToken.and.returnValue( myFakeAccessToken );

    // getRefreshToken spy
    getRefreshToken = authServiceSpy.getRefreshToken.and.returnValue( myFakeRefreshToken );

    // Logout spy
    logoutSpy = authServiceSpy.logout.and.returnValue();

    // Configure test module
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [        
        { provide: AuthService, useValue: authServiceSpy }, // Inject spy authService instead of the original one
        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptor,
          multi: true,
        }
        
      ],  
    })

    // Init http objects
    httpClient = TestBed.get(HttpClient);
    httpMock = TestBed.get(HttpTestingController);
  });


  /*
    Test the injection of the token in request header
    Should transmit the reponse of the original request
  */
  it('should inject token', () => {
    const fakeData = 'myFakeData';
    // Make an HTTP GET request
    httpClient.get<any>(testUrl).subscribe(response => {
      // Check that interceptor resend me the good response
      expect(response).toEqual(fakeData);
    });

    // The following `expectOne()` will match the request's URL.
    const req: TestRequest = httpMock.expectOne(testUrl);

    // Check that the interceptor inject the token in the header
    const headers = req.request.headers
    expect(headers.has(authorizationHeaderKey)).toBeTrue();
    expect(headers.get(authorizationHeaderKey)).toEqual('Bearer ' + myFakeAccessToken);

    expect(getJwtTokenSpy).toHaveBeenCalled();

    // Respond with mock 201 
    req.flush(fakeData, { status: 201, statusText: 'Created' });
  });



  /*
    Test throw bad credentials error
    If the http response from api/token request has 401 http error
  */
  it('should bad credentials', () => {

    // Send acces token request
    httpClient.get<any>(environment.urls.accessToken).subscribe(response => {
      // Should never be here
      fail('Should never received the response of the http get')
    }, 
    (error => {
      // Should receive expired session error
      expect(error instanceof BadCredentialsError).toBeTrue();
    }));


    // get req mock
    const reqFake: TestRequest = httpMock.expectOne(environment.urls.accessToken);

    // Respond with mock 401 
    reqFake.flush('Bad Request', { status: 401, statusText: 'Unauthorized' });
  })



  /*
    Test the call of refresh token when received a 401 http error
    Should inject the new access token 
    Finally, recall the orignal request and transmit the response
  **/
  it('should refresh token', () => {
    const fakeData = 'myFakeData';

    // Make an HTTP GET request
    httpClient.get<any>(testUrl).subscribe(response => {
      // Check that interceptor resend the good response after the refreh token
      expect(response).toEqual(fakeData);
    });

    // get req mock
    const reqFake: TestRequest = httpMock.expectOne(testUrl);

    // Mock refresh token
    const newAccessToken = 'newaccesstoken';
    refreshTokenSpy = authServiceSpy.refreshToken.and.returnValue( of({access: newAccessToken}));

    // Respond with mock 401 
    reqFake.flush('Bad Request', { status: 401, statusText: 'Unauthorized' });

    // Should call refresh token
    expect(refreshTokenSpy).toHaveBeenCalled();

    // A new original request should be call
    const newReqFake: TestRequest = httpMock.expectOne(testUrl);

    // Check that the interceptor inject the new token in the header
    const headers = newReqFake.request.headers
    expect(headers.has(authorizationHeaderKey)).toBeTrue();
    expect(headers.get(authorizationHeaderKey)).toEqual('Bearer ' + newAccessToken);

    expect(getJwtTokenSpy).toHaveBeenCalled();

    // Respond with mock 201 
    newReqFake.flush(fakeData, { status: 201, statusText: 'Created' });
  })


  /*
    Test the expired session
    If interceptor receive 401 error code after for refreshToken request 
    Then logout and throw error cancelling the original request
  */
  it('should expired session', () => {
    // Make an HTTP GET request
    httpClient.get<any>(testUrl).subscribe(response => {
      // Should never be here
      fail('Should never received the response of the http get')
    }, 
    (err) => {
      // Should receive expired session error
      expect(err instanceof ExpiredSessionError).toBeTrue();
    });

    // get req mock
    const reqFake: TestRequest = httpMock.expectOne(testUrl);

    // Mock refresh token => throw 401 error
    refreshTokenSpy = authServiceSpy.refreshToken.and.returnValue(httpClient.get<any>(environment.urls.refreshToken));

    // Send 401 for the orginal request
    reqFake.flush('Bad Request', { status: 401, statusText: 'Unauthorized' });

    // Should call refresh token
    expect(refreshTokenSpy).toHaveBeenCalled();

    // A refresh request should be call
    const refreshReq: TestRequest = httpMock.expectOne(environment.urls.refreshToken);

    // Send 401 error with the good error code
    const error = {code: environment.httpErrorCode.invalid_token}
    refreshReq.flush(error, {status: 401, statusText: 'Unauthorized' });

    // Should logout
    expect(logoutSpy).toHaveBeenCalled();
  })
});
