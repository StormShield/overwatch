import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';


/**
 * Interceptor to add the current language on the request URL
 *
 * @export
 * @class LanguageInterceptoref
 * @implements {HttpInterceptor}
 */
@Injectable()
export class LanguageInterceptor implements HttpInterceptor {

  public static readonly PREFERENCE_LANGUAGE = 'PREFERENCE_LANGUAGE';
  public static readonly DEFAULT_LANGUAGE = 'en';

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // Inject lang only on request to django server
    if(request.url.includes(environment.apiUrl)) {
      request = this.addLang(request, LanguageInterceptor.getLanguage());
    }
    return next.handle(request);

  }


  /**
   * Add language in url request
   *
   * @private
   * @param {HttpRequest<unknown>} request
   * @param {string} lang : the lang
   * @returns {HttpRequest<unknown>}
   * @memberof LanguageInterceptor
   */
  private addLang(request: HttpRequest<unknown>, lang: string): HttpRequest<unknown> {
    // Insert lang
    let url = environment.apiUrl + '/' + lang + '/';
    request.url.split(environment.apiUrl).forEach(value => url += value);

    // Return clone request
    return request.clone({
      url: url
    })
  }


  /**
   * Get the current language
   * The one set on session or if not exists the default one
   *
   * @static
   * @memberof LanguageInterceptor
   */
  public static getLanguage() {
    let lang = localStorage.getItem(LanguageInterceptor.PREFERENCE_LANGUAGE);
    return lang ? lang : LanguageInterceptor.DEFAULT_LANGUAGE;

  }
  

}
