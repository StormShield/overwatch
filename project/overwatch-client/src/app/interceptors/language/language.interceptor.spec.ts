import { TestBed } from '@angular/core/testing';

import { LanguageInterceptor } from './language.interceptor';
import { TranslateStubsModule } from 'src/app/test/stubs/translate.stubs.module';
import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule, TestRequest } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

describe('LanguageInterceptor', () => {

  // For http test
  let httpClient: HttpClient;
  let httpMock: HttpTestingController;

  
  beforeEach(() => {
  
    TestBed.configureTestingModule({
      imports: [TranslateStubsModule, HttpClientTestingModule],
      providers: [
        LanguageInterceptor,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: LanguageInterceptor,
          multi: true,
        }]
      })
  
    httpClient = TestBed.get(HttpClient);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    const interceptor: LanguageInterceptor = TestBed.inject(LanguageInterceptor);
    expect(interceptor).toBeTruthy();
  });

  /*
    Test sending a request to another server
    The interceptor should do nothind
  */
  it('should do nothing', () => {
    // Send request with other base url
    const fakeUrl = 'localfake:5555/api/'
    httpClient.get<any>(fakeUrl).subscribe();

    // Get the request => should be with the same url
    const testReq: TestRequest = httpMock.expectOne(fakeUrl);
    expect(testReq.request.url).toEqual(fakeUrl);
    testReq.flush('Success');
  })

  /*
    Test add language
    Send request with good url base
    Check getting language in local storage or defaultLanguage
  */
  it('should add language', () => {
    // Default language should be 'en'
    expect(LanguageInterceptor.DEFAULT_LANGUAGE).toEqual('en');

    // Mock localStorage with null
    localStorage.getItem =  jasmine.createSpy().and.returnValue(null);
    const suffixUrl = "test/path/";
    
    //send request
    httpClient.get<any>(environment.apiUrl + suffixUrl).subscribe();

    // Get the request => should be modify with default language
    let testReq: TestRequest = httpMock.expectOne(environment.apiUrl + '/' + LanguageInterceptor.DEFAULT_LANGUAGE + '/' + suffixUrl);
    expect(testReq.request.url).toEqual(environment.apiUrl + '/' + LanguageInterceptor.DEFAULT_LANGUAGE + '/' + suffixUrl);
    expect(localStorage.getItem).toHaveBeenCalledWith(LanguageInterceptor.PREFERENCE_LANGUAGE);
    testReq.flush('Success');

    // Mock localStorage with 'fr'
    localStorage.getItem =  jasmine.createSpy().and.returnValue('fr');

    //send request
    httpClient.get<any>(environment.apiUrl + suffixUrl).subscribe();

    // Get the request => should be modify with fr  language
    testReq = httpMock.expectOne(environment.apiUrl + '/fr/' + suffixUrl);
    expect(testReq.request.url).toEqual(environment.apiUrl + '/fr/' + suffixUrl);
    expect(localStorage.getItem).toHaveBeenCalledWith(LanguageInterceptor.PREFERENCE_LANGUAGE);
    testReq.flush('Success');
  })
});
