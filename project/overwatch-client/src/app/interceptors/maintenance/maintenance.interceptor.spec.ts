import { TestBed } from '@angular/core/testing';

import { MaintenanceInterceptor } from './maintenance.interceptor';
import { Router } from '@angular/router';
import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule, TestRequest } from '@angular/common/http/testing';
import { MaintenanceError } from 'src/app/models/errors/maintenance.error';

describe('MaintenanceInterceptor', () => {

  let httpClient: HttpClient;
  let httpMock: HttpTestingController;

  // Spies
  let routerSpy;
  let navigateSpy;

  const testUrl = 'test/arch'

  beforeEach(() =>  {
    
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
    navigateSpy = routerSpy.navigateByUrl.and.returnValue(Promise.resolve(true));
    
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: Router, useValue: routerSpy },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: MaintenanceInterceptor,
          multi: true,
        }
        ]
    });

    // Init http objects
    httpClient = TestBed.get(HttpClient);
    httpMock = TestBed.get(HttpTestingController);
  })
  


  /*
    Test : interceptor do nothing if no http response error
  */
  it('should do nothing if not error', () => {
    const responseExpected  = 'myansweris'
    // Send request
    httpClient.get<any>(testUrl).subscribe(response => {
      expect(response).toEqual(responseExpected);
      expect(navigateSpy).toHaveBeenCalledTimes(0);
    });
    
    // The following `expectOne()` will match the request's URL.
    const testReq: TestRequest = httpMock.expectOne(testUrl);
    testReq.flush(responseExpected);

    navigateSpy.calls.reset();
  })

  /*
    Test : interceptor rethrow error if the error has no 503 status
  */
  it('should rethrow error', () => {
    const responseExpected  = 'myansweris'
    // Send request
    httpClient.get<any>(testUrl).subscribe(response => {
      fail('Should never get response from the request')
    },
    (err) => {
      expect(navigateSpy).toHaveBeenCalledTimes(0);
      expect(err.error).toEqual(responseExpected);
    });
    
    // The following `expectOne()` will match the request's URL.
    const testReq: TestRequest = httpMock.expectOne(testUrl);
    testReq.flush(responseExpected, {status: 401, statusText: 'unauthorized'});

    navigateSpy.calls.reset();
  })

    /*
    Test : interceptor redirect and rethrow error if the error has 503 status
  */
  it('should redirect and rethrow error', () => {
    const responseExpected  = 'myansweris'
    // Send request
    httpClient.get<any>(testUrl).subscribe(response => {
      fail('Should never get response from the request')
    },
    (err) => {
      expect(err instanceof MaintenanceError).toEqual(true);
      expect(navigateSpy).toHaveBeenCalledTimes(1);
      expect(navigateSpy).toHaveBeenCalledWith('/home/maintenance');
    });
    
    // The following `expectOne()` will match the request's URL.
    const testReq: TestRequest = httpMock.expectOne(testUrl);
    testReq.flush(responseExpected, {status: 503, statusText: 'Service unvailable'});

    navigateSpy.calls.reset();
  })



});
