import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MaintenanceError } from 'src/app/models/errors/maintenance.error';

/**
 * Intercept maintenance error from server to redirect to the maintenance page
 *
 * @export
 * @class MaintenanceInterceptor
 * @implements {HttpInterceptor}
 */
@Injectable()
export class MaintenanceInterceptor implements HttpInterceptor {

  constructor(private router: Router) {}


  /**
   * Redirect to the maintenance page if the server answer by 503 error
   *
   * @param {HttpRequest<unknown>} request
   * @param {HttpHandler} next
   * @returns {Observable<HttpEvent<unknown>>}
   * @memberof MaintenanceInterceptor
   */
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // Send the request
    return next.handle(request).pipe(
      // Handle maintenance error 503
      catchError((error, caught) => {
        if(error instanceof HttpErrorResponse && error.status === 503) {
          this.router.navigateByUrl('/home/maintenance');
          return throwError(new MaintenanceError());
        }
        else {
          return throwError(error);
        }     
      })
    );
  }
}
