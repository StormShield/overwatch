import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { LanguageInterceptor } from './interceptors/language/language.interceptor';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  // Version app
  version: string;

  constructor(public translate: TranslateService) {
    // Add supported languages in the translate service
    translate.addLangs(['en', 'fr']);
    translate.setDefaultLang(LanguageInterceptor.getLanguage());
  }


  ngOnInit(): void {
    this.version = environment.version;
  }
  title = 'overwatch-client';


}
