// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  version: '0.0.0.0',

  apiUrl: 'http://localhost:8000',

  // Suffix api address
  urls: {
    refreshToken: 'api/token/refresh/',
    accessToken: 'api/token/',
    maintenance: 'maintenance/',
    anomaly: 'anomaly/'
  },

  // Error http code from server
  httpErrorCode: {
    invalid_token: 'token_not_valid'
  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
