export const environment = {
  production: true,
  version: '${version}', // Should be replace by GitLab for release master

  apiUrl: 'http://localhost:8000',

  // Suffix api address
  urls: {
    refreshToken: 'api/token/refresh/',
    accessToken: 'api/token/',
    maintenance: 'maintenance/',
    anomaly: 'anomaly/'
  },

  // Error http code from server
  httpErrorCode: {
    invalid_token: 'token_not_valid'
  }
};
