# OverwatchClient
This project is a front end client to manage anomalies plane registration.

It's have been made with Angular 9 technology.

## Prerequisite
You need this differents tools to install :

[npm](https://www.npmjs.com/get-npm) : npm is dependencies manager

[Angular CLI](https://github.com/angular/angular-cli) A command line interface for angular

[OverwatchServer](https://gitlab.com/StormShield/overwatch/-/tree/master/project%2Foverwatch-server): The Overwatch server side providing a rest framwork API


## Development

### Server

Run `ng serve` for start a developement server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Production

### Build Electron project in for production 

Run `npm run-script electron-build-prod` to build the project and inject it on [Electron](https://www.electronjs.org/). The build artifact will be stored in the `electron/dist` directory.

### Electron package (for windows)

Run `npm run-script electron-app-package` to create a package of the application with an .exe (only for windows). The package will be stored in the `electron/delivery/overwatch-win32-x64` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).


## Documentation
Please find the documentation [HERE](https://gitlab.com/StormShield/overwatch/-/wikis/Documentation) (Not already written)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Contact
Developer: nicolas.brimont@protonmail.com
