import { app, BrowserWindow } from 'electron'
import * as path from 'path'
import * as url from 'url'


/**
 * Entry point to create a web browser electron and to inject it the app
 * 
 * See Tutorial : https://malcoded.com/posts/angular-desktop-electron/
 */



/**
 * Window to host our angular app
 */
let win: BrowserWindow;



// Trigger when the app is ready
app.on('ready', createWindow);



// Create new brower window in an chromium app
function createWindow() {
    win = new BrowserWindow({ width: 800, height: 600 });

    win.loadURL(
        url.format({
          pathname: path.join(__dirname, `/../../dist/overwatch-client/index.html`),
          protocol: 'file:',
          slashes: true,
        })
      );
    
      //win.webContents.openDevTools();

    // Emitted when the window is closed.
    // See : https://gist.github.com/sidprice/612cb49cec923eeb94cfcddf1736c181
    win.on('closed', function() {
      // Dereference the window object, usually you would store windows
      // in an array if your app supports multi windows, this is the time
      // when you should delete the corresponding element.
      win = null
  })
}

  // Close the app when the all windows close
app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
});

