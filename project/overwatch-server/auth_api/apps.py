from django.apps import AppConfig


### Authentification API to give JWT token and refresh token
###
###
class AuthApiConfig(AppConfig):
    name = 'auth_api'
