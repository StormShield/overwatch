from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from django.contrib.auth.models import User
import logging


# Redefine token JWT serializer to custom renderer
# Add on the token :
#   - username : name of the user
#   - isAdmin : true if the authenticate user is admin; Else otherwise
class OwTokenObtainPairSerializer(TokenObtainPairSerializer):

     @classmethod
     def get_token(cls, user: User):
        """[summary]
        Redefine token JWT serializer to custom renderer
        Add on the token :
        - username : name of the user
        - isAdmin : true if the authenticate user is admin; Else otherwise
        
        Arguments:
            TokenObtainPairSerializer -- serializer of the simplejwt lib
            user {User} -- user asking for the token
        
        Returns:
            [type] -- the access token and the refrsh token
        """
        logging.warning("ACESS API : Get_Token")


        # Get the generate token by superclass
        token = super().get_token(user)

        # Add custom claims
        token['username'] = user.username
        #  # To know is the user is an admin
        token['isAdmin'] = user.is_staff


        return token