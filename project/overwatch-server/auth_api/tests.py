import json
import jwt
import logging
from django.test import TestCase, Client
from rest_framework import status
from django.contrib.auth.models import User, UserManager
from auth_api.serializers import OwTokenObtainPairSerializer
from django.urls import reverse
from rest_framework_simplejwt import tokens
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import activate




# Create your tests here.

# initialize the APIClient app
client = Client()


# Test the access token api
# Sources :
# - https://docs.djangoproject.com/fr/3.0/topics/testing/advanced/django
# - https://realpython.com/test-driven-development-of-a-django-restful-api/#post
# - https://docs.djangoproject.com/fr/3.0/topics/testing/tools/
class GetAccessToken(TestCase):
    """ Test module for GET access token """

    # Set up env test
    def setUp(self):
        """ Set up model before tests
        """

        # Activate language en
        activate('en')

        # Basic user
        self.basicuser = User.objects.create_user(
            username='basic_user', email='basic_user@…', password='basic_user_pwd')
        
        # Admin user
        self.adminuser = User.objects.create_user(username='admin_user', email='admin_user@...', password='admin_user_pwd', is_staff=True)

        # Data basic user request
        self.basic_user_data = {
            'username': 'basic_user',
            'password': 'basic_user_pwd'
        }

        # Data admin user request
        self.admin_user_data = {
            'username': 'admin_user',
            'password': 'admin_user_pwd'
        }

        #basicUser = UserManager._create_user(UserManager, 'test_token_usr', '', 'test_token_pwd')
        #User.objects.create(basicUser)

    # Test get token api
    def test_get_token_credential(self):
        """ - Test get token and refresh token from api with credentials
            - Check that token contains username and isStaff from user informations
        """
        logging.debug('Start Test : test_get_token_credential')

        users = [self.basicuser, self.adminuser]
        users_data = [self.basic_user_data, self.admin_user_data]

        for i in range(len(users)):
            data = users_data[i]
            user: User = users[i]
            

            # get API response
            response = client.post(reverse('token_obtain_pair'), data=json.dumps(data), content_type='application/json')

            # response ok
            self.assertEqual(response.status_code, status.HTTP_200_OK)

            # Deserialize response content
            content = response.json()

            # Gets tokens
            access_token = content['access']
            refresh_token = content['refresh']

            # Decode token
            access_decoded = jwt.decode(access_token, settings.SECRET_KEY, algorithms=['HS256'])

            # Check access token contains username and isadmin correct information
            self.assertEqual(access_decoded['username'], user.get_username())
            self.assertEqual(access_decoded['isAdmin'], user.is_staff)

        logging.debug('End Test : test_get_token_credential')



    # Test test_get_token_wrong_credential api
    def test_get_token_wrong_credential(self):
        """ - Test to get token with wrong credentials. Should be return status_code 401
        """ 
        logging.debug('Start Test : test_get_token_wrong_credential')

        # wrong data
        data = {
            'username': 'wrong_user',
            'password': 'wrong_user_pwd'
        }

        # get API response
        response = client.post(reverse('token_obtain_pair'), data=json.dumps(data), content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        logging.debug('End Test : test_get_token_wrong_credential')
    
    # Test get token with refresh token
    def test_get_token_with_refresh_token(self):
        """ Obain a token with credential then get token with the refresh token
        """

        # get API response
        response = client.post(reverse('token_obtain_pair'), data=json.dumps(self.basic_user_data), content_type='application/json')

        # response ok
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        refresh_token = response.json()['refresh']

        # Get new token with refresh token
        response = client.post(reverse('token_refresh'), data=json.dumps({'refresh': refresh_token}), content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)


