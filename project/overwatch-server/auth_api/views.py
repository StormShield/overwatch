from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt import utils
from auth_api.serializers import OwTokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
import logging
from maintenance_api.permissions import MaintenanceAccessPermission
from django.utils.translation import ugettext as _
from django.conf import settings
import os


# Define custom ow view to get jwt token
class OwTokenObtainPairView(TokenObtainPairView):
    """ Custom serializer of the view to obain pair token
    - Herits: TokenObtainPairView    """
    # Set new serializer token
    serializer_class = OwTokenObtainPairSerializer