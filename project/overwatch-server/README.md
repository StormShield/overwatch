# OverwatchServer (for windows)
OverwatchServer is a product that offer a rest framework api to manage anomalies plane registration.
It's using Pyhon/Django technologies

## Prerequisite
You need this differents tools to install :

[python](https://www.python.org/downloads/) : install last version of python.

[pip](https://pip.pypa.io/en/stable/) : to manage python dependencies.

[virtualenv](https://virtualenv.pypa.io/en/latest/) : to create virtual python env.

You need a postgresql (https://www.postgresql.org/) database install in your environement:

    - Should have a postgressql user allowed to create and modify database.

    - Set the credentials in the settings.py user and password ['DATABASES']=>'default'^

    - The database server should be running in local and listening on port 5432

## Installation 
    You should be in overwatch-server root path

### Create env
Use virtualenv to create your virtual environement
```bash
virtualenv python_env
```
Then activate your environement
```bash
python_env\Scripts\Activate
```
### Dependencies
Install overwatch project dependencies. All the dependencies need has write been in the requirements.txt file
```bash
pip install -r requirements.txt
```

### Migrate database
All the databse configuration has been already configurate.

Juste use this command to migrate the configuration on database

```bash
python manage.py migrate
```

### Create superuser
Create a super user by using this command and follow the instruction.

```bash
python manage.py createsuperuser
```

### Start server
You can start server with the following command.

WARNING: the server is only running in developement mode. The server is not yet already for production

```bash
python manage.py runserver
```

The server is by default running on port 8000

### Create new user
You can create new user on database by using the admin interface.

With you favorite browser join this [ADMIN_URL](http://localhost:8000/en/admin/).

You can now create different user. There is Admin and Basic user on this app. To create an admin user you should check "Staff status" during user creation procedure


## Running unit tests
You can run the uniary tests
```bash
python manage.py test
```
Please make sure to update tests as appropriate.

## Try It 
You can play with the server using the client side [OverwatchClient](https://gitlab.com/StormShield/overwatch/-/tree/master/project%2Foverwatch-client) project

## Documentation
Please find the documentation [HERE](https://gitlab.com/StormShield/overwatch/-/wikis/Documentation) (Not already written)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Contact
Developer: nicolas.brimont@protonmail.com
