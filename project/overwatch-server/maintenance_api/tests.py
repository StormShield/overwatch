import json
import datetime
from django.test import TestCase, Client
from django.contrib.auth.models import User
from unittest.mock import Mock, PropertyMock
from unittest.mock import patch
from django.urls import reverse
from maintenance_api.permissions import MaintenanceAccessPermission
from django.http.request import HttpRequest
from rest_framework import status
from rest_framework.request import Request
from rest_framework.test import APITestCase
from django.utils import timezone
from rest_framework_simplejwt.tokens import RefreshToken
from django.utils.translation import activate
from overwatch.api_exceptions import MaintenanceException
import logging

# initialize the APIClient app
client = Client()

# Maintenance test
class MaintenanceTest(APITestCase):
    """ All the maintenance api test """

    # Set up env test
    def setUp(self):
        """ Set up model before tests
        """
        # Activate language en
        activate('en')

        # Basic user
        self.basicuser = User.objects.create_user(
            username='basic_user', email='basic_user@…', password='basic_user_pwd')
        
        # Admin user
        self.adminuser = User.objects.create_user(username='admin_user', email='admin_user@...', password='admin_user_pwd', is_staff=True)

        # Data basic user request
        self.basic_user_data = {
            'username': 'basic_user',
            'password': 'basic_user_pwd'
        }

        # Data admin user request
        self.admin_user_data = {
            'username': 'admin_user',
            'password': 'admin_user_pwd'
        }

    @patch('maintenance_mode.core.get_maintenance_mode')
    def test_get_maintenance(self, mock_get_maintenance):
        """ Test Get Maintenance.
            Basic user should be unauthorized to access of this api
            Admin should get the maintenance server status
         """
        logging.info('Start Test : test_get_maintenance')

        # Create A Token manually
        token = RefreshToken.for_user(self.basicuser)

        #######################################
        ##### FORBIDDEN TEST #################
        #####################################
        # Set Authorization Header
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer ' + str(token.access_token),
        }

        response = client.get(reverse('maintenance'), format='json', **auth_headers)
        # Make assertions
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        #######################################
        ##### AUTHORIZED TEST #################
        #####################################
        # Do the same for an admin user
        token = RefreshToken.for_user(self.adminuser)
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer ' + str(token.access_token),
        }
        # Set maintenant to off
        mock_get_maintenance.return_value = False
        response = client.get(reverse('maintenance'), format='json', **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Deserialize response content
        content = response.json()
        self.assertEqual(content['status'], False)

        # Set maintenant to on
        mock_get_maintenance.return_value = True
        response = client.get(reverse('maintenance'), format='json', **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = response.json()
        self.assertEqual(content['status'], True)

        logging.info('End Test : test_get_maintenance')

    @patch('maintenance_mode.core.set_maintenance_mode')
    def test_put_maintenance(self, mock_set_maintenance): 
        """ Test Put Maintenance
            Basic user should be unauthorized to access of this api
            Admin could set the maintenance server status
        """
        logging.info('Start Test : test_post_maintenance')

        # Create A Token manually
        token = RefreshToken.for_user(self.basicuser)



        #######################################
        ##### FORBIDDEN TEST #################
        #####################################
       
        # Set Authorization Header
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer ' + str(token.access_token),
        }

        response = client.post(reverse('maintenance'), format='json', **auth_headers)
        # Make assertions
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(mock_set_maintenance.call_count, 0)

        #######################################
        ##### AUTHORIZED TEST #################
        #####################################
        #Do the same for an admin user
        token = RefreshToken.for_user(self.adminuser)
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer ' + str(token.access_token),
        }

        # Put to set maintenance in 'on' mode
        response = client.put(reverse('maintenance') + 'on', format='json', **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(mock_set_maintenance.call_count, 1)
        mock_set_maintenance.assert_called_once_with(True)

        mock_set_maintenance.reset_mock()

        # Put to set maintenance in 'off' mode
        response = client.put(reverse('maintenance') + 'off', format='json', **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(mock_set_maintenance.call_count, 1)
        mock_set_maintenance.assert_called_once_with(False)

        mock_set_maintenance.reset_mock()

        # Put call with wrong argument
        response = client.put(reverse('maintenance') + 'badarg', format='json', **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(mock_set_maintenance.call_count, 0)

        logging.info('End Test : test_post_maintenance')



class MaintenancePermissionsTest(TestCase):

        # Set up env test
    def setUp(self):
        """ Set up model before tests
        """
        # Basic user
        self.basicuser = User.objects.create_user(username='basic_user', email='basic_user@…', password='basic_user_pwd')
        # Admin user
        self.adminuser = User.objects.create_user(username='admin_user', email='admin_user@...', password='admin_user_pwd', is_staff=True)

    @patch('maintenance_mode.core.get_maintenance_mode')
    @patch('django.contrib.auth.models.User.is_staff')
    def test_has_permission(self, mock_is_staff, mock_get_maintenance):
        """ Test Maintenance Permission

        If maintenant mode = true then only admin (staff) has access
        If maintenance mmode = false then every users has access
        
        Arguments:
            mock_is_staff -- Mock on user.i_staff
            mock_get_maintenance -- Mock on get_maintenance_mode
        """
        logging.info('Start Test : test_has_permission')

        # Class to test
        permission = MaintenanceAccessPermission()

        fakeRequest = Request(HttpRequest())
        fakeRequest.user = self.basicuser

        #Fake view
        fakeView = None

        mock_get_maintenance.return_value = False
        self.assertTrue(permission.has_permission(fakeRequest, fakeView))

        mock_get_maintenance.return_value = True
        with self.assertRaises(MaintenanceException) as vre:
            permission.has_permission(fakeRequest, fakeView)

        fakeRequest.user = self.adminuser

        mock_get_maintenance.return_value = False
        self.assertTrue(permission.has_permission(fakeRequest, fakeView))

        mock_get_maintenance.return_value = True
        self.assertTrue(permission.has_permission(fakeRequest, fakeView))

        logging.info('End Test : test_has_permission')



