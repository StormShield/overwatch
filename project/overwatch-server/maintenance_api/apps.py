from django.apps import AppConfig


class MaintenanceApiConfig(AppConfig):
    name = 'maintenance_api'
