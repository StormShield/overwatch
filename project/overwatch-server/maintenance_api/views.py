from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from maintenance_mode import core
from overwatch.api_exceptions import WrongFormatException
from rest_framework import status

import logging

# Create your views here.



class MaintenanceView(APIView):
    serializer_class = None
    http_method_names = ["get", "put"]

    # Set auth
    permission_classes = (IsAuthenticated, IsAdminUser,)

    # /maintenance/
    def get(self, request):
        """ Get the maintenance status
        
        Arguments:
            request  -- HTTP request
          Returns:
            response -- HTTP response with the actual maintenance status
        """

        logging.info('Get_Maintenance')

        status_maintenance: bool = core.get_maintenance_mode()
        response: Response = Response(data={'status': status_maintenance}, status=status.HTTP_200_OK)
    
        return response

    # /maintenante/<status_maintenance> 
    # status_maintenance = on | off
    def put(self, request, status_maintenance):
        """ Change the maintenance status
        
        Arguments:
            request  -- HTTP request
            status_maintenance [str] -- the new maintenance status - on/off
        
        Returns:
            response -- HTTP response
        """

        logging.info('Post_Maintenance')
        if status_maintenance == 'on':
            core.set_maintenance_mode(True)
        elif status_maintenance == 'off':
            core.set_maintenance_mode(False)
        else:
            raise WrongFormatException

        return Response(status=status.HTTP_204_NO_CONTENT)

