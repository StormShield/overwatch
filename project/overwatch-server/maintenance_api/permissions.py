from rest_framework import permissions
from maintenance_mode import core
from django.contrib.auth.models import User
from overwatch.api_exceptions import MaintenanceException
import logging


class MaintenanceAccessPermission(permissions.BasePermission):

    label = 'flatpage_permission'

    def has_permission(self, request, view):
        """ Maintenance permission
    
            If the server is in maintenance mode then only staff user has permission
        
        Arguments:
            request  -- HTTP request
            view  -- View ask by the request
        
        Returns:
            [bool] -- true if has permission. Else otherwise
        """
        logging.info('Maintenance access permission')

        if(core.get_maintenance_mode() and not request.user.is_staff):
            raise MaintenanceException
        else:
            return True