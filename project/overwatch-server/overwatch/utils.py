#### Define here all the utils for the overwatch project

class Interval:
    """ An interval between a min and max value """
    min: int
    max: int

    def __init__(self, min, max):
        self.min = min
        self.max = max