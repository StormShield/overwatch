"""overwatch URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import path
from django.conf.urls.i18n import i18n_patterns
from rest_framework_simplejwt import views as jwt_views
from rest_framework_swagger.views import get_swagger_view
from auth_api import views as ow_auth_views
from anomaly_api.views import AnomalyView
from maintenance_api import views as maintenance_views

schema_view = get_swagger_view(title="Notes API")

urlpatterns =  i18n_patterns(
    path('api/docs/', schema_view),
    path('admin/', admin.site.urls),
    path('api/token/', ow_auth_views.OwTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('api/token/verify/', jwt_views.TokenVerifyView.as_view(), name='token_verify'),
    path('maintenance/', maintenance_views.MaintenanceView.as_view(), name='maintenance'),
    path('maintenance/<status_maintenance>', maintenance_views.MaintenanceView.as_view(), name='maintenance'),
    path('anomaly/', AnomalyView.as_view(), name='anomaly'),
)