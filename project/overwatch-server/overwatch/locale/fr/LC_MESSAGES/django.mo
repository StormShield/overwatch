��          �            x     y  G   �  @   �  1     0   @  1   q  4   �  2   �  6        B     P  /   c     �     �     �  _  �     ?  M   Q  E   �  9   �  8     9   X  8   �  6   �  :        =     O  0   c     �      �  !   �                                                                    	       
    Bad_Request Ensure that there are no more than {max_decimal_places} decimal places. Ensure that there are no more than {max_digits} digits in total. Ensure the X value is less than or equal to 1000. Ensure the y value is less than or equal to 500. Ensure the z value is less than or equal to 1000. Ensure this x value is greater than or equal to 500. Ensure this y value is greater than or equal to 0. Ensure this z value is greater than or equal to -1000. Hello, World! Maintenance_Server The server is in maintenace. Please retry later This field is required. This field may not be null. Wrong format of request Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-03-01 23:09+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Mauvaise requête Assurez vous qu'il n'y est pas plus de {max_decimal_places} décimal au total Assurez vous qu'il n'y est pas plus de {max_digits} décimal au total Verifiez que X à une valeur inférieur ou égale à 1000 Verifiez que y à une valeur inférieur ou égale à 500 Verifiez que z à une valeur inférieur ou égale à 1000 Verifiez que X à une valeur supérieur ou égale à 500 Verifiez que y à une valeur supérieur ou égale à 0 Verifiez que z à une valeur supérieur ou égale à -1000 Bonjour le monde! Maintenance_Serveur Le serveur est en maintenance. Essayez plus tard Ce champs est requis Ce champs ne doit pas être null La requête est au mauvais format 