from rest_framework.exceptions import APIException
from django.utils.translation import ugettext as _


class WrongFormatException(APIException):
    """ Wong format exception
    this exception is raise if the data gave by the client api has a bad format
    
    Herit:
        APIException
    """
    status_code = 400
    #. Translators: Response message return if the request has the wrong format
    default_detail = _('Wrong format of request')
    default_code = _('Bad_Request')


class WrongFormatFieldException(APIException):
    """ Wrong format field exception
    Get the errors dictionnary generate by rest_framework and transform it in wrong format exception

    Herit:
        APIException 
    """
    status_code = 400
    default_code = _('Bad_Request')

    def __init__(self, errors):
        """ Init wrong format field exception
        Get a dict of errors and transformat it in in api exception
        Arguments:
            errors {[Dict]} -- errors from rest_framework validation serializer
        """
        super().__init__(None)

class MaintenanceException(APIException):
    """ This exception should be raise if the server is in maintenance 
    and the user is not admin
    """

    status_code = 503
    #. Translators: Http error if the server is in maintenance
    default_detail = _('The server is in maintenace. Please retry later')
    default_code = _('Maintenance_Server')
