
from enum import Enum

class DefaultType(Enum):
    """ The existing different type if default on a structure

    Herit: IntEnum
    """
    PEELING = 'Peeling'
    POROSITY = 'Porosity'
    CRACK = 'Crack'
  
    # @classmethod
    # def choices(cls):
    #     return [(key.value, key.name) for key in cls]
