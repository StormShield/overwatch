from django.shortcuts import render
from rest_framework.views import APIView
from .models import Anomaly
from .serializers import AnomalySerializer
from rest_framework.response import Response
from rest_framework import status
from maintenance_api.permissions import MaintenanceAccessPermission
from rest_framework.permissions import IsAuthenticated

# Create your views here.
class AnomalyView(APIView):

    # Set auth
    permission_classes = (IsAuthenticated, MaintenanceAccessPermission)

    http_method_names = ["get", "post"]


    def get(self, request, format=None):
        """[summary]
        Get all the anomalies
        Arguments:
            request  -- HTTP request
        
        Keyword Arguments:
            format  -- data format (default: {None})
        
        Returns:
            [Response] -- response with all anomalies in database
        """
        anomalies = Anomaly.objects.all()
        serializer = AnomalySerializer(anomalies, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        """[summary]
        
        Arguments:
            request {[type]} -- [description]
        
        Keyword Arguments:
            format {[type]} -- [description] (default: {None})
        
        Returns:
            [type] -- [description]
        """
        serializer = AnomalySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)