from django.apps import AppConfig


class AnomalyApiConfig(AppConfig):
    name = 'anomaly_api'
