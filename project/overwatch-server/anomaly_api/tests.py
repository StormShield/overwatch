from django.test import TestCase, Client
from django.core.validators import ValidationError
from django.utils.translation import activate
from django.urls import reverse
from django.contrib.auth.models import User

from .models import Coordinate, Anomaly
from .serializers import AnomalySerializer, CoordinateSerializer
from .utils import DefaultType
from overwatch.api_exceptions import WrongFormatFieldException

from rest_framework import status
from rest_framework.renderers import JSONRenderer
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.test import APITestCase

from unittest.mock import Mock, PropertyMock
from unittest.mock import patch
import logging

# Create your tests here.


class CoordinateModelTest(TestCase):
    """ Anomaly Model Testy """

    def test_x_validator_interval(self):
        """ Test that x interval validation works """
        logging.info('Start Test: test_x_validator_interval')
        
        # Check validator interval value of coordinate

        # X MAX
        with self.assertRaises(ValidationError) as vre:
            c1 = Coordinate(x=0, y=200, z=500)
            c1.clean_fields(exclude=None)
        
        #X MIN
        with self.assertRaises(ValidationError) as vre:
            c1 = Coordinate(x=1200, y=200, z=500)
            c1.clean_fields(exclude=None)

        #X IN
        try:
            c1 = Coordinate(x=500, y=200, z=500)
            c1.clean_fields(exclude=None)
        except ValidationError:
            self.fail("X validator raise un unexpected Exception")

    def test_y_validator_interval(self):
        """ Test that y interval validation works """
        logging.info('Start Test: test_y_validator_interval')

        # Y MAX
        with self.assertRaises(ValidationError) as vre:
            c1 = Coordinate(x=600, y=600, z=500)
            c1.clean_fields(exclude=None)
        
        #Y MIN
        with self.assertRaises(ValidationError) as vre:
            c1 = Coordinate(x=600, y=-20, z=500)
            c1.clean_fields(exclude=None)

        #Y IN
        try:
            c1 = Coordinate(x=600, y=0, z=500)
            c1.clean_fields(exclude=None)
        except ValidationError:
            self.fail("Y validator raise un unexpected Exception")

    def test_z_validator_interval(self):
        """ Test that z interval validation works """
        logging.info('Start Test: test_z_validator_interval')

        # Z MAX
        with self.assertRaises(ValidationError) as vre:
            c1 = Coordinate(x=600, y=200, z=2000)
            c1.clean_fields(exclude=None)
        
        #Z MIN
        with self.assertRaises(ValidationError) as vre:
            c1 = Coordinate(x=600, y=200, z=-1001)
            c1.clean_fields(exclude=None)

        #Z IN
        try:
            c1 = Coordinate(x=600, y=200, z=1000)
            c1.clean_fields(exclude=None)
        except ValidationError:
            self.fail("Z validator raise un unexpected Exception")


    def test_coordinate_creation(self):
        """ Test the good creation of a coordinate in the database
        """
        coordinate = Coordinate.objects.create(x=600, y=200, z=20)
        self.assertTrue(
                Coordinate.objects.filter(x="600").exists()
        )

    


class AnomalyModelTest(TestCase): 

    # Set up env test
    def setUp(self):
        self.c1 = Coordinate(x=600, y=200, z=20)

    def test_anomaly_model(self):
        """ Test the anomaly model
        """
        # Create coordinate
        coordinate = Coordinate.objects.create(x=self.c1.x, y=self.c1.y, z=self.c1.z)

        # Test coordinate creation
        a1 = Anomaly.objects.create(coordinate=coordinate, defaultType=DefaultType.CRACK)
        query = Anomaly.objects.filter(defaultType=DefaultType.CRACK)
        self.assertTrue(query.exists())
        aCreated = query.first()
        self.assertEqual(aCreated.get_default_type_label(), 'Crack')
        self.assertEqual(aCreated.coordinate.x, coordinate.x)
        self.assertEqual(aCreated.coordinate.y, coordinate.y)
        self.assertEqual(aCreated.coordinate.z, coordinate.z)

        # Test delete with cascade
        uniqueCoordinateQuery = Coordinate.objects.filter(id=coordinate.id)
        self.assertTrue(uniqueCoordinateQuery.exists())
        uniqueCoordinateQuery.delete()
        self.assertEqual(uniqueCoordinateQuery.count(), 0)

        uniqueAnomalyQuery = Anomaly.objects.filter(coordinate=coordinate)
        self.assertEqual(uniqueAnomalyQuery.count(), 0)


class AnomalySerializerTest(TestCase):
    
    def test_serialization(self):
        """ Test to serialize and deserialize data 
        """
        ### json data
        data_anomaly = { 
            'coordinate': { 'x' : 600, 'y' : 200, 'z' : 0 },
            'defaultType': DefaultType.PEELING.value,
            'comment': 'this is a comment'
        }

        ### Create anomaly from data
        serializer = AnomalySerializer(data=data_anomaly)
        if serializer.is_valid():
            serializer.create(validated_data=serializer.validated_data)
        else: self.fail('Data should be valid')

        query = Anomaly.objects.filter(defaultType=DefaultType.PEELING)
        self.assertTrue(query.exists())

        ### Get anomaly from instance to json
        createdAnomaly = query.first()
        serializer = AnomalySerializer(instance=createdAnomaly)
        jsonAnomaly = JSONRenderer().render(serializer.data)
        self.assertEqual(serializer.data.pop('defaultType'), DefaultType.PEELING.value)
        self.assertEqual(serializer.data.pop('comment'), 'this is a comment')
        self.assertEqual(JSONRenderer().render(serializer.data['coordinate']), b'{"x":"600.00","y":"200.00","z":"0.00"}')

        
    # def test_wrong_format_field_exception(self):
    #     errorsDict = {
    #         "coordinate": {
    #             "x": [
    #                 "Vérifiez que cette valeur est supérieur ou égale à 500"
    #             ],
    #             "z": [
    #                 "Vérifiez que cette valeur est supérieur ou égale à -1000"
    #             ]
    #         },
    #         "defaultType": [
    #             "This field is required."
    #         ]
    #     }

    #     exception = WrongFormatFieldException(errorsDict)



# initialize the APIClient app
client = Client()

class AnomalyViewTest(APITestCase):

    # Set up env test
    def setUp(self):
        """ Set up model before tests
        """
        # Activate language en
        activate('en')

        # Basic user
        self.basicuser = User.objects.create_user(
            username='basic_user', email='basic_user@…', password='basic_user_pwd')
        
        # Data basic user request
        self.basic_user_data = {
            'username': 'basic_user',
            'password': 'basic_user_pwd'
        }


    @patch('maintenance_mode.core.get_maintenance_mode')
    def test_get_anomalies(self, mock_get_maintenance):
        """ Test case to get all the         
        Arguments:
            mock_get_maintenance {[type]} -- [description]
        """
        logging.info('Start Test : test_get_anomalies')

        # set mock on 'off' maintenance
        mock_get_maintenance.return_value = False

        # Create A Token manually
        token = RefreshToken.for_user(self.basicuser)

        # Construct http header
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer ' + str(token.access_token),
        }

        # Get anomalies => should be 0
        response = client.get(reverse('anomaly'), format='json', **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

        # Insert two anomalies
        coordinate1 = Coordinate.objects.create(x=500, y=600, z=2)
        coordinate2 = Coordinate.objects.create(x=655, y=841, z=-215)
        anomaly1 = Anomaly.objects.create(coordinate=coordinate1, defaultType=DefaultType.CRACK)     
        anomaly2 = Anomaly.objects.create(coordinate=coordinate2, defaultType=DefaultType.PEELING) 

        # Get anomalies => should be 2
        response = client.get(reverse('anomaly'), format='json', **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)


    @patch('maintenance_mode.core.get_maintenance_mode')
    def test_post_anomalies(self, mock_get_maintenance):
        """ Test post anomaly on api view
        Arguments:
            mock_get_maintenance -- mock api to have off maintenance mode
        """
        # set mock on 'off' maintenance
        mock_get_maintenance.return_value = False

        # Create A Token manually
        token = RefreshToken.for_user(self.basicuser)

        # Construct http header
        auth_headers = {
            'HTTP_AUTHORIZATION': 'Bearer ' + str(token.access_token),
        }

        # Good Json data anomaly
        data_anomaly = {
            'coordinate': { 'x' : 600, 'y' : 300, 'z' : -1000 },
            'defaultType': 'Peeling',
            'comment': 'This is the 2nd anomaly'
        }

        bad_data_anomaly = {
            "coordinate": { "x" : 200, "y" : 300, "z" : -1000 },
            'defaultType': 'Peeling',
            "comment": "This is the 2nd anomaly"
        }

        # Send request with bad data => should get error http status
        response = client.post(reverse('anomaly'), data=bad_data_anomaly, format='json', content_type='application/json', **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # Get anomaly un database to check
        anomaly_db = Anomaly.objects.filter(defaultType=DefaultType.PEELING)
        self.assertFalse(anomaly_db.exists())

        # Send request => should register new data on database
        response = client.post(reverse('anomaly'), data=data_anomaly, format='json', content_type='application/json', **auth_headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Get anomaly un database to check
        anomaly_db = Anomaly.objects.filter(defaultType=DefaultType.PEELING)
        self.assertTrue(anomaly_db.exists())






