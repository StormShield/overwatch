from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from .utils import DefaultType
from django.utils.translation import gettext_lazy as _, ngettext_lazy
from django_enum_choices.fields import EnumChoiceField
from overwatch.utils import Interval

# Create your models here.


class Coordinate(models.Model):
   """ Coordinate in 3 dimensional plan
      x : decimal
      y : decimal
      z : decimal
   
   Returns:
       [type] -- [description]
   """

   # Coordinates
   x = models.DecimalField(null=False, max_digits=9, decimal_places=2,
      validators=[
            MaxValueValidator(1000, message=_('Ensure the X value is less than or equal to 1000.')),
            MinValueValidator(500, message=_('Ensure this x value is greater than or equal to 500.'))
         ]
   )
   y = models.DecimalField(null=False, max_digits=9, decimal_places=2,
      validators=[
            MaxValueValidator(500, message=_('Ensure the y value is less than or equal to 500.')),
            MinValueValidator(0, message=_('Ensure this y value is greater than or equal to 0.'))
         ]
   )
   z = models.DecimalField(null=False, max_digits=9, decimal_places=2,
      validators=[
            MaxValueValidator(1000, message=_('Ensure the z value is less than or equal to 1000.')),
            MinValueValidator(-1000, message=_('Ensure this z value is greater than or equal to -1000.'))
         ]
   )

   def _str__(self):
            return '(' + str(self.x) + ', ' + str(self. y) + ', ' + str(self.z) + ')'

class Anomaly(models.Model):
   """ Anomaly model compose of :
      Coordinate
      DefaultType
      Comment
   """
   # Interval coodinates constraints
   coordinate = models.OneToOneField(Coordinate, on_delete=models.CASCADE, null=False)
   defaultType = EnumChoiceField(DefaultType, null=False)
   comment = models.TextField(null=True)

   def get_default_type_label(self):
      """Get the label of the default type
      
      Returns:
          [str] -- the label type
      """
      return DefaultType(self.defaultType).name.title()


   class Meta:
      verbose_name = "anomaly"

   def _str__(self):
      return self.coordinate._str__() + ' Default: ' + self.get_default_type_label()