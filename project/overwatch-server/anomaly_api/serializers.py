from rest_framework import serializers
from django_enum_choices.serializers import EnumChoiceModelSerializerMixin
from .models import Anomaly, Coordinate
from django_enum_choices.fields import EnumChoiceField
from .utils import DefaultType

##### Here the serialize Django model to JSON
class CoordinateSerializer(serializers.ModelSerializer):
    class Meta:
            model = Coordinate
            fields = ['x', 'y', 'z']

        

class AnomalySerializer(EnumChoiceModelSerializerMixin, serializers.ModelSerializer):
    """Serializer for Anomaly model
    """
    coordinate = CoordinateSerializer(read_only=False)
    defaultType = EnumChoiceField(DefaultType)


    class Meta:
        model = Anomaly
        fields = ('coordinate', 'defaultType', 'comment')

    ### Redefine create method for the nested coordinate field
    def create(self, validated_data):
        # create coordinate
        coordinate_validated_data = validated_data.pop('coordinate')
        coordinate_serializer = self.fields['coordinate']
        coordinate = coordinate_serializer.create(coordinate_validated_data)

        # create anomaly with the previous created coordinate and the validated_data
        anomaly = Anomaly.objects.create(coordinate=coordinate, **validated_data)
        return anomaly