# Overwatch
Overwatch in an ambitous project to manage all the anomalies on airplanes

## OverwatchServer
The server has been create with the Python/Django technology. It's providing an API rest that offer tools to register planes anomalies

See the [OverwatchServer](https://gitlab.com/StormShield/overwatch/-/tree/master/project%2Foverwatch-server) project for more informations.

## OverwatchClient
The client has been create with Angular 9 technology. It can be embedded in a electron app to be use as a Windows 10 application. 

It communicate with the API server to helping the user to manage airplanes anomalies

See the [OverwatchClient](https://gitlab.com/StormShield/overwatch/-/tree/master/project%2Foverwatch-client) project for more informations.

## Documentation
Please find the documentation [HERE](https://gitlab.com/StormShield/overwatch/-/wikis/Documentation) (Not already written)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Contact
Developer: nicolas.brimont@protonmail.com